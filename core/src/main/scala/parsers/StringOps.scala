package parsers

import scala.annotation.tailrec

object StringOps {

  @tailrec
  def split(s: String, delimiter: Char, result: Seq[String] = Seq.empty): Seq[String] = {
    val idx = s.indexOf(';')
    if (idx >= 0) {
      val (token, tail) = s.splitAt(idx)
      split(tail.drop(1), delimiter, result :+ token)
    } else {
      result :+ s
    }
  }

  def removeQuotes(s: String): String = s.stripPrefix("\"").stripSuffix("\"")

}
