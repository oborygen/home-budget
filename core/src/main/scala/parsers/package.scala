import java.time.LocalDate
import java.time.format.DateTimeFormatter

import cats.data._
import cats.implicits._

import scala.util.Try

package object parsers {

  case class ParsingContext(numberFormat: NumberParser, dateFormat: DateTimeFormatter)

  type NumberParser = String => Option[BigDecimal]

  type Interpreter[O] = (String, ParsingContext) => Either[String, O]

  type Validator[O] = (String, ParsingContext) => Validated[String, O]

  implicit class ValidateString(s: String) {

    def as[O](implicit validator: Validator[O], ctx: ParsingContext): ValidatedNel[String, O] =
      validator(s, ctx).toValidatedNel

  }

  implicit class ValidateOptional(o: Option[String]) {

    def required[O](implicit validator: Validator[O], ctx: ParsingContext): ValidatedNel[String, O] =
      o match {
        case Some(value) => value.as[O]
        case None        => "Missing required value".invalidNel
      }

    def optional[O](
        implicit validator: Validator[O], ctx: ParsingContext): ValidatedNel[String, Option[O]] =
      o match {
        case Some(value) => value.as[O].map(Option(_))
        case None        => None.validNel
      }

  }

  val dateInterpreter: Interpreter[LocalDate] = (s, ctx) => {
    if(s.trim.isEmpty) {
      Left("Empty string for date")
    } else {
      Try(LocalDate.parse(s, ctx.dateFormat)).toEither.leftMap(_.getMessage)
    }
  }

  val intInterpreter: Interpreter[Int] = (s, _) => {
    if(s.trim.isEmpty) {
      Left("Empty string for number")
    }  else {
      Try(s.toInt).toEither.leftMap(_.getMessage)
    }
  }

  def decimalFormatInstance(decimalSeparator: Char, groupingSeparator: Option[Char] = None): NumberParser = s => {
    val (base, decimals) = s.span(_ != decimalSeparator)
    val baseNormalized = groupingSeparator match {
      case Some(sep) => base.replace(sep.toString,"")
      case None => base
    }
    if( !(baseNormalized.matches("-?[0-9]+") && decimals.drop(1).matches("[0-9]+"))) {
      None
    } else {
      Some(BigDecimal(baseNormalized + "." + decimals.drop(1)))
    }
  }

  val decimalInterpreter: Interpreter[BigDecimal] = (s, ctx) => {
    if(s.isEmpty) {
      Left("Empty string for number")
    } else {
      ctx.numberFormat(s) match {
        case Some(bd) => Right(bd)
        case None => Left(s"Invalid number $s")
      }
    }
  }

  implicit val dateValidator: Validator[LocalDate] = (s, ctx) =>
    dateInterpreter(s, ctx).toValidated

  implicit val intValidator: Validator[Int] = (s, ctx) =>
    intInterpreter(s, ctx).toValidated

  implicit val decimalValidator: Validator[BigDecimal] = (s, ctx) =>
    decimalInterpreter(s, ctx).toValidated

  implicit val stringValidator: Validator[String] = (s, _) => s.valid

}
