package parsers

import java.time.LocalDate
import java.time.format.DateTimeFormatter

import com.typesafe.scalalogging.StrictLogging
import domain.Transaction
import parsers.StringOps._

import scala.annotation.tailrec
import scala.io.Codec

//TODO: move to a module
//TODO: IO it
object IngTransactionsParser extends StrictLogging {

  val defaultEncoding: Codec = "Cp1250"

  import cats.implicits._

  private val TRANSACTION_ID_COL = "Nr transakcji"

  private val TRANSACTION_DATE_COL = "Data transakcji"

  private val BOOKING_DATE_COL = "Data księgowania"

  private val TITLE_COL = "Tytuł"

  private val AMOUNT_COL = "Kwota transakcji (waluta rachunku)"

  private val COUNTERPARTY_COL = "Dane kontrahenta"

  private case class Context(endOfData: Boolean = false, manifest: FileManifest, tail: Stream[String])

  private case class FileManifest(columns: List[String] = List.empty, complete: Boolean = false)

  implicit val ctx: ParsingContext = ParsingContext(
    decimalFormatInstance(','),
    DateTimeFormatter.ISO_DATE
  )

  @tailrec
  private def parseLines(context: Context, transactions: Stream[Transaction]): Iterator[Transaction] =
    context match {
      case Context(false, FileManifest(_, false), head #:: tail) if head.trim.isEmpty =>
        parseLines(context.copy(tail = tail), transactions)
      case Context(false, manifest @ FileManifest(_, false), head #:: tail) =>
        val manifestUpdated = parseManifest(head, manifest)
        parseLines(context.copy(tail = tail, manifest = manifestUpdated), transactions)
      case Context(false, FileManifest(_, true), tail) if tail.isEmpty =>
        transactions.iterator
      case Context(false, FileManifest(columns, true), head #:: tail) =>
        parseTransaction(head, columns) match {
          case Left(error) =>
            logger.warn(s"Invalid transaction $error")
            parseLines(context.copy(tail = tail), transactions)
          case Right(transaction) =>
            parseLines(context.copy(tail = tail), transactions :+ transaction)
        }
      case Context(true, _, _) =>
        transactions.iterator
    }

  def parseTransaction(line: String, columns: List[String]): Either[String, Transaction] = {
    val values = split(line, ';').map(removeQuotes)
    val valuesMap = columns.zip(values).toMap
    (
      valuesMap.get(TRANSACTION_ID_COL).required[String],
      valuesMap.get(TRANSACTION_DATE_COL).required[LocalDate],
      valuesMap.get(BOOKING_DATE_COL).required[LocalDate],
      valuesMap.get(TITLE_COL).required[String],
      valuesMap.get(COUNTERPARTY_COL).required[String],
      valuesMap.get(AMOUNT_COL).required[BigDecimal]
    ).mapN(Transaction).toEither.leftMap(_.toString)
  }

  private def parseManifest(line: String, manifest: FileManifest): FileManifest =
    if (line.startsWith("\"Data transakcji\"")) {
      val columns =
        split(line, ';').toList.map(removeQuotes)
      manifest.copy(complete = true, columns = columns)
    } else {
      manifest
    }

  def parse(lines: Stream[String]): Iterator[Transaction] = {
    parseLines(Context(false, FileManifest(), lines), Stream.empty)
  }

}
