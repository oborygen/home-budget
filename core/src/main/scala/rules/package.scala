import java.util.UUID

import domain.Transaction

package object rules {

  type CategoryId = UUID

  type Rule = PartialFunction[Transaction, CategoryId]

}
