package rules

import org.parboiled2._
import rules.Algebra._

class RuleParser(val input: ParserInput) extends Parser {

  def InputLine = rule { Term ~ EOI }

  def OperationRule: Rule1[Operator] = rule {
    LikeRule | ContainsRule
  }

  def Term: Rule1[Operator] = rule {
    Factor ~ zeroOrMore(
      WS ~ "AND" ~ WS ~ Factor ~> ((l: Operator, r: Operator) => And(l, r))
        | WS ~ "OR" ~ WS ~ Factor ~> ((l: Operator, r: Operator) => Or(l, r)))
  }

  def WS = rule { zeroOrMore(anyOf(" \t\f")) }

  def Factor = rule { OperationRule | Parens }

  def Parens = rule { WS ~ '(' ~ WS ~ Term ~ WS ~ ')' ~ WS ~> ((p: Operator) => Parenthesis(p)) }

  def Number = rule { capture(Digits) ~> (_.toInt) }

  def Digits = rule { oneOrMore(CharPredicate.Digit) }

  def Polish = CharPredicate('Ą' to 'ż') ++ 'ó' ++ 'Ó'

  def Text = rule {
    WS ~ '"' ~ capture(oneOrMore(CharPredicate.from(_ != '"'))) ~ '"' ~ WS |
      WS ~ '\'' ~ capture(oneOrMore(CharPredicate.from(_ != '\''))) ~ '\'' ~ WS
  }

  def LikeRule: Rule1[Operator] = rule {
    StringFieldRule ~ WS ~ "like" ~ WS ~ Text ~> ((fld: StringField, pattern: String) => Like(fld, pattern))
  }

  def ContainsRule: Rule1[Operator] = rule {
    StringFieldRule ~ WS ~ "contains" ~ WS ~ Text ~> ((fld: StringField, pattern: String) => Contains(fld, pattern))
  }

  def StringFieldRule: Rule1[StringField] = rule { capture("title" | "counterparty") ~> ((f: String) => StringField(f)) }

  def DateFieldRule: Rule1[DateField] = rule { capture("bookingDate" | "transactionDate") ~> ((f: String) => DateField(f)) }

  def NumberFieldRule: Rule1[NumericField] = rule { capture("amount") ~> ((f: String) => NumericField(f))}
}
