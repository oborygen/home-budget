package rules

import domain.Transaction

class SimpleRule(f: Transaction => Option[CategoryId]) extends Rule {

  override def isDefinedAt(x: Transaction): Boolean = f(x).isDefined

  override def apply(v1: Transaction): CategoryId = f(v1) match {
    case Some(cat) => cat
    case None      => throw new MatchError(v1)
  }
}
