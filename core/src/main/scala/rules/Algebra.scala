package rules

import java.time.LocalDate

import domain.Transaction

object Algebra {

  sealed trait Operator {
    def eval(t: Transaction): Boolean
  }

  case class And(left: Operator, right: Operator) extends Operator {
    override def eval(ctx: Transaction): Boolean =
      left.eval(ctx) && right.eval(ctx)
  }
  case class Or(left: Operator, right: Operator) extends Operator {
    override def eval(ctx: Transaction): Boolean =
      left.eval(ctx) || right.eval(ctx)
  }
  case class MatchingRule(operator: Operator)

  case class Parenthesis(term: Operator) extends Operator {
    override def eval(ctx: Transaction): Boolean =
      term.eval(ctx)
  }

  case class Negation(part: Operator) extends Operator {
    override def eval(ctx: Transaction): Boolean =
      !part.eval(ctx)
  }

  sealed trait Field[O] {
    def eval(ctx: Transaction): O
  }

  case class StringField(field: String) extends Field[String] {
    override def eval(ctx: Transaction): String = field match {
      case "title" => ctx.title
      case "counterparty" => ctx.counterparty
    }
  }

  case class DateField(field: String) extends Field[LocalDate] {
    override def eval(ctx: Transaction): LocalDate = field match {
      case "transactionDate" => ctx.transactionDate
      case "bookingDate" => ctx.bookingDate
    }
  }

  case class NumericField(field: String) extends Field[BigDecimal] {
    override def eval(ctx: Transaction): BigDecimal = field match {
      case "amount" => ctx.value
    }
  }

  case class Like(field: Field[String], pattern: String) extends Operator {

    val regex = pattern
      .replace("*", ".*")
      .replace("?", ".")

    override def eval(t: Transaction): Boolean = field.eval(t).matches("(?i)" + regex)
  }

  case class Contains(field: Field[String], fragment: String) extends Operator {
    override def eval(t: Transaction): Boolean = field.eval(t).toLowerCase.contains(fragment.toLowerCase)
  }

}
