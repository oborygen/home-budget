package rules

import domain.{Category, Transaction}

class Categorizer(rulesFinder: Transaction => Iterable[Rule]) {

  def categorize(transaction: Transaction): Option[CategoryId] = {
    val potentialCategorizers = rulesFinder(transaction)
    potentialCategorizers.find(_.isDefinedAt(transaction)).map(_(transaction))
  }

}