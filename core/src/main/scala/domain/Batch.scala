package domain

import java.time.{LocalDate, LocalDateTime}
import java.util.UUID

case class Batch(
    id: UUID,
    processingTime: LocalDateTime,
    transactionsCount: Int,
    existingCount: Int,
    categorizedCount: Int,
    unallocatedCount: Int,
    allocatedCount: Int)
