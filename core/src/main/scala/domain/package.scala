import cats.MonadError

package object domain {

  type ThrowableMonadError[M[_]] = MonadError[M, Throwable]
}
