package domain

import java.time.LocalDate

case class Goal(name: String,
                start: LocalDate,
                finish: LocalDate,
                amount: BigDecimal)
