package domain

import java.util.UUID

case class Category(id: UUID, name: String, group: Option[UUID])

object Category {

  val default: Category = Category(
    UUID.fromString("717DAB09-4A79-48E4-BE05-D3619213FB90"),
    "Unknown",
    None)
}
