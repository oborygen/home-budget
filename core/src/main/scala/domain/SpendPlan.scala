package domain

import java.time.LocalDate
import java.util.UUID

case class SpendPlan(id: UUID,
                     categories: Set[UUID],
                     start: LocalDate,
                     interval: PlanningInterval,
                     amount: BigDecimal
                 )
