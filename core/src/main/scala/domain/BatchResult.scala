package domain

import java.util.UUID

case class BatchResult(
    id: UUID,
    existing: Iterable[Transaction], //already existing
    uncategorized: Iterable[ProcessedTransaction],
    unallocated: Iterable[ProcessedTransaction],
    allocations: List[(SpendAllocation, List[ProcessedTransaction])])
