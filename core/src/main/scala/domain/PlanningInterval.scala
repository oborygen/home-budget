package domain

import java.time.temporal.TemporalAdjusters
import java.time.{DayOfWeek, LocalDate, Period}

sealed trait PlanningInterval {
  def range(date: LocalDate): (LocalDate, LocalDate)
  def period: Period
}

case object Weekly extends PlanningInterval {

  val period: Period = Period.ofWeeks(1)

  override def range(date: LocalDate): (LocalDate, LocalDate) =
    (
      date.`with`(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY)),
      date.`with`(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY))
    )
}
case object Monthly extends PlanningInterval {
  override def range(date: LocalDate): (LocalDate, LocalDate) =
    (
      date.`with`(TemporalAdjusters.firstDayOfMonth()),
      date.`with`(TemporalAdjusters.lastDayOfMonth())
    )

  override def period: Period = Period.ofMonths(1)
}
case object Quarterly extends PlanningInterval {
  override def range(date: LocalDate): (LocalDate, LocalDate) = {
    val start = LocalDate.of(date.getYear, date.getMonth.firstMonthOfQuarter(), 1)
    val end = start.plusMonths(2).`with`(TemporalAdjusters.lastDayOfMonth())
    (start, end)
  }

  override def period: Period = Period.ofMonths(3)
}
case object Yearly extends PlanningInterval {
  override def range(date: LocalDate): (LocalDate, LocalDate) =
    (
      date.`with`(TemporalAdjusters.firstDayOfYear()),
      date.`with`(TemporalAdjusters.lastDayOfYear())
    )

  override def period: Period = Period.ofYears(1)
}
