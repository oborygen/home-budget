package domain

import java.time.{LocalDate, LocalDateTime}
import java.util.UUID

case class SpendAllocation(id: UUID,
                           spendPlanId: UUID,
                           batchId: Option[UUID],
                           lastUpdate: LocalDateTime,
                           intervalStart: LocalDate,
                           intervalEnd: LocalDate,
                           amount: BigDecimal) {

  def increase(transactionAmount: BigDecimal): SpendAllocation = copy(amount = amount + transactionAmount)

}
