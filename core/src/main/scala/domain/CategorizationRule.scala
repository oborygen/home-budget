package domain

import java.util.UUID

case class CategorizationRule(id: UUID, expression: String, categoryId: UUID)
