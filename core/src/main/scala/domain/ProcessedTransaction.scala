package domain

import java.util.UUID

//TODO possible extension to split transaction across multiple categories
case class ProcessedTransaction(transaction: Transaction, categoryId: Option[UUID], batchId: UUID, allocated: Boolean = false)
