package domain

import java.time.LocalDate

case class Transaction(id: String,
                       transactionDate: LocalDate,
                       bookingDate: LocalDate,
                       title: String,
                       counterparty: String,
                       value: BigDecimal)
