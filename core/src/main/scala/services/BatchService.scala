package services

import java.time.{LocalDate, LocalDateTime}
import java.util.UUID

import cats.implicits._
import domain._
import repository.{AllocationRepository, BatchRepository, SpendPlanRepository, TransactionRepository}

import scala.annotation.tailrec

class BatchService[M[_]: SpendPlanRepository: AllocationRepository: TransactionRepository: BatchRepository](
    categorizer: Transaction => Option[UUID])(implicit H: ThrowableMonadError[M]) {

  private def partitionByExisting(transactions: Iterable[Transaction]): M[(List[Transaction], List[Transaction])] = {
    import repository.TransactionRepository.syntax._
    val zippedM: M[List[(Boolean, Transaction)]] = transactions
      .map(transaction => transactionIsAllocated[M](transaction.id).map(_ -> transaction))
      .toList
      .sequence
    for {
      zipped <- zippedM
    } yield {
      zipped.collect { case (false, t) => t } -> zipped.collect { case (true, t) => t }
    }
  }

  @tailrec
  private def doAllocations(
      transactions: List[ProcessedTransaction],
      plans: Array[SpendPlan],
      allocations: List[(SpendAllocation, List[ProcessedTransaction])])
    : List[(SpendAllocation, List[ProcessedTransaction])] =
    transactions match {
      case head :: tail => doAllocations(tail, plans, applyAllocations(head, allocations, plans))
      case Nil          => allocations
    }

  private def applyAllocations(
      transaction: ProcessedTransaction,
      allocations: List[(SpendAllocation, List[ProcessedTransaction])],
      plans: Array[SpendPlan]): List[(SpendAllocation, List[ProcessedTransaction])] = {
    val matchingPlansIds = plans.collect {
      case plan if plan.categories.exists(transaction.categoryId.contains) => plan.id
    }
    allocations.map {
      case (allocation, matchingTransactions)
          if matchingPlansIds.contains(allocation.spendPlanId)
            && !allocation.intervalStart.isAfter(transaction.transaction.transactionDate)
            && !allocation.intervalEnd.isBefore(transaction.transaction.transactionDate) =>
        allocation.increase(transaction.transaction.value) ->
          (matchingTransactions :+ transaction.copy(allocated = true))
      case noMatch => noMatch
    }
  }

  private def resolveAllocations(
      transactions: Iterable[ProcessedTransaction],
      plans: Array[SpendPlan]): M[List[SpendAllocation]] = {
    val allocationIndices: List[(UUID, (LocalDate, LocalDate))] = (for {
      transaction <- transactions
      plan <- plans.filter(_.categories.exists(transaction.categoryId.contains))
    } yield {
      (plan.id, plan.interval.range(transaction.transaction.transactionDate))
    }).toList.distinct
    allocationIndices.map { case (id, (start, end)) => AllocationApi.findAllocation(id, start, end) }.sequence
  }

  def processBatch(transactions: Iterable[Transaction]): M[BatchResult] = {
    import repository.SpendPlanRepository.syntax._

    val batchId = UUID.randomUUID()

    def categorize(transaction: Transaction): ProcessedTransaction =
      ProcessedTransaction(transaction, categorizer(transaction), batchId)

    for {
      plans <- allPlans.map(_.toArray)
      partitioned <- partitionByExisting(transactions)
      (newTransactions, existing) = partitioned
      withCategory = newTransactions.map(categorize)
      (categorized, uncategorized) = withCategory.partition { case ProcessedTransaction(_, cat, _, _) => cat.isDefined }
      allocations <- resolveAllocations(categorized, plans)
    } yield {
      val allocated = doAllocations(categorized, plans, allocations.map(_ -> List.empty))
      val allocatedTransactionIds = allocated.flatMap(_._2).map(_.transaction.id).toSet
      val unallocated = categorized.filter(t => !allocatedTransactionIds.contains(t.transaction.id))
      BatchResult(batchId, existing, uncategorized, unallocated, allocated)
    }
  }

  def saveBatch(result: BatchResult): M[Unit] = {
    import repository.TransactionRepository.syntax._
    import repository.AllocationRepository.syntax._
    import repository.BatchRepository.syntax._
    val BatchResult(id, existing, uncategorized, unallocated, allocated) = result
    val allocatedSize = allocated.flatMap(_._2).size
    val batch = Batch(
      id,
      LocalDateTime.now,
      transactionsCount = existing.size + uncategorized.size + unallocated.size + allocatedSize,
      existingCount = existing.size,
      categorizedCount = allocatedSize + unallocated.size,
      unallocatedCount = unallocated.size,
      allocatedCount = allocatedSize
    )
    for {
      _ <- uncategorized.map(saveTransaction(_)).toList.sequence
      _ <- allocated
        .map(_._1)
        .map(a => increaseAllocation(a.spendPlanId, id, LocalDateTime.now, a.intervalStart, a.intervalEnd, a.amount))
        .sequence
      _ <- (allocated.flatMap(_._2) ++ unallocated).map(saveTransaction(_)).sequence
      _ <- save(batch)
    } yield {
      ()
    }
  }

}
