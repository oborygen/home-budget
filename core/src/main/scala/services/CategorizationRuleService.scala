package services

import cats.implicits._
import com.typesafe.scalalogging.LazyLogging
import domain.{CategorizationRule, ThrowableMonadError, Transaction}
import repository.{CategoryRepository, RuleRepository}
import rules.Algebra.Operator
import rules.{CategoryId, Rule, RuleParser}

import scala.util.{Failure, Success}

object CategorizationRuleService extends LazyLogging {

  import repository.RuleRepository.syntax

  def loadRules[M[_] : RuleRepository : ThrowableMonadError](): M[List[Rule]] = {
     syntax.loadRules.map { rules =>
      for {
        rule <- rules
        rulePredicate <- parserExpression(rule.expression)
      } yield {
        val pf: PartialFunction[Transaction, CategoryId] = {
          case t: Transaction if rulePredicate.eval(t) => rule.categoryId
        }
        pf
      }
    }
  }

  private def parserExpression(expression: String): Option[Operator] = {
    new RuleParser(expression).InputLine.run() match {
      case Failure(e) =>
        logger.warn(s"Invalid rule expression $expression")
        None
      case Success(op) =>
        Some(op)
    }
  }

  def addRule[M[_] : RuleRepository : CategoryRepository](rule: CategorizationRule)(implicit H: ThrowableMonadError[M]): M[Unit] = {
    import CategoryRepository.syntax._
    for {
      cat <- categoryExists[M](rule.categoryId)
      _ <- if(cat) {
        RuleRepository.syntax.addRule(rule)
      } else {
        H.raiseError(new RuntimeException(s"Category ${rule.categoryId} is not defined"))
      }
    } yield {
      ()
    }
  }
}
