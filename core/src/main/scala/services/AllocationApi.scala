package services

import java.time.{LocalDate, LocalDateTime}
import java.util.UUID

import cats.implicits._
import domain._
import repository.AllocationRepository

object AllocationApi {

  import repository.AllocationRepository.syntax._

  def findAllocation[M[_]: AllocationRepository](planId: UUID, start: LocalDate, end: LocalDate)(
      implicit H: ThrowableMonadError[M]): M[SpendAllocation] =
    for {
      allocationO <- loadLatestAllocation(planId, start, end)
      allocation = getOrCreateAllocation(allocationO, planId, start, end)
    } yield {
      allocation
    }

  def getOrCreateAllocation[M[_]](
      allocationO: Option[SpendAllocation],
      planId: UUID,
      start: LocalDate,
      end: LocalDate): SpendAllocation =
    allocationO.getOrElse(SpendAllocation(UUID.randomUUID(), planId, None, LocalDateTime.now, start, end, 0))

}
