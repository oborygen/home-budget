package repository

import java.time.{LocalDate, LocalDateTime}
import java.util.UUID

import domain.SpendAllocation

trait AllocationRepository[F[_]] {
  def loadLatest(planId: UUID, start: LocalDate, end: LocalDate): F[Option[SpendAllocation]]
  def save(allocation: SpendAllocation): F[Unit]

  def increaseAmount(
      spendPlanId: UUID,
      batchId: UUID,
      lastUpdate: LocalDateTime,
      intervalStart: LocalDate,
      intervalEnd: LocalDate,
      amount: BigDecimal): F[Unit]

  def deleteBatch(batchId: UUID): F[Int]
}

object AllocationRepository {

  object syntax {

    def loadLatestAllocation[F[_]](planId: UUID, start: LocalDate, end: LocalDate)(
        implicit repo: AllocationRepository[F]): F[Option[SpendAllocation]] = repo.loadLatest(planId, start, end)

    def saveAllocation[F[_]](allocation: SpendAllocation)(implicit repo: AllocationRepository[F]): F[Unit] =
      repo.save(allocation)

    def increaseAllocation[F[_]](
        spendPlanId: UUID,
        batchId: UUID,
        lastUpdate: LocalDateTime,
        intervalStart: LocalDate,
        intervalEnd: LocalDate,
        amount: BigDecimal)(implicit repo: AllocationRepository[F]): F[Unit] =
      repo.increaseAmount(spendPlanId, batchId, lastUpdate, intervalStart, intervalEnd, amount)

    def deleteBatch[F[_]](batchId: UUID)(implicit repo: AllocationRepository[F]): F[Int] = repo.deleteBatch(batchId)
  }
}
