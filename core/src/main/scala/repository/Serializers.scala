package repository

import java.time.{LocalDate, LocalDateTime}
import java.util.UUID

import cats.implicits._
import domain.{Category, ProcessedTransaction, SpendAllocation, Transaction}
import io.circe._
import io.circe.generic.semiauto._

object Serializers {

  implicit val uuidCodec: Decoder[UUID] = Decoder.decodeString.emap { str =>
    Either.catchNonFatal(UUID.fromString(str)).leftMap(_ => "Not UUID")
  }
  implicit val dateCodec: Decoder[LocalDate] = Decoder.decodeString.emap { str =>
    Either.catchNonFatal(LocalDate.parse(str)).leftMap(_ => "Not a valid date")
  }
  implicit val dateTimeCodec: Decoder[LocalDateTime] = Decoder.decodeString.emap { str =>
    Either.catchNonFatal(LocalDateTime.parse(str)).leftMap(_ => s"Not a valid date and time: $str")
  }

  implicit val spendAllocationDecoder: Decoder[SpendAllocation] = Decoder.instance { h =>
    for {
      id <- h.get[UUID]("id")
      spentPlan <- h.get[UUID]("spendPlan")
      batchId <- h.get[UUID]("batchId")
      lastUpdate <- h.get[LocalDateTime]("lastUpdate")
      intervalStart <- h.get[LocalDate]("intervalStart")
      intervalEnd <- h.get[LocalDate]("intervalEnd")
      amount <- h.get[BigDecimal]("amount")
    } yield {
      SpendAllocation(id, spentPlan, Some(batchId), lastUpdate, intervalStart, intervalEnd, amount)
    }
  }

  implicit val spendAllocationEncoder: Encoder[SpendAllocation] = deriveEncoder

  implicit val transactionDecoder: Decoder[Transaction] = deriveDecoder[Transaction]

  implicit val transactionEncoder: Encoder[Transaction] = deriveEncoder[Transaction]

  implicit val categoryEncoder: Encoder[Category] = deriveEncoder[Category]

  implicit val categoryDecoder: Decoder[Category] = deriveDecoder[Category]

  implicit val processedTransactionDecoder: Decoder[ProcessedTransaction] = deriveDecoder

  implicit val processedTransactionEncoder: Encoder[ProcessedTransaction] = deriveEncoder
}
