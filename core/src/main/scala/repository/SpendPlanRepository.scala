package repository

import java.util.UUID

import domain.SpendPlan

trait SpendPlanRepository[F[_]] {
  def load(id: UUID): F[Option[SpendPlan]]
  def allPlans: F[Set[SpendPlan]]
  def save(spendPlan: SpendPlan): F[Unit]
}

object SpendPlanRepository {

  object syntax {

    def loadSpendPlan[F[_]](planId: UUID)(implicit repo: SpendPlanRepository[F]): F[Option[SpendPlan]] =
      repo.load(planId)

    def allPlans[F[_]](implicit repo: SpendPlanRepository[F]): F[Set[SpendPlan]] =
      repo.allPlans

    def save[F[_]](spendPlan: SpendPlan)(implicit repo: SpendPlanRepository[F]): F[Unit] =
      repo.save(spendPlan)
  }
}
