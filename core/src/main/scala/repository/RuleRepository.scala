package repository

import domain.CategorizationRule

trait RuleRepository[F[_]] {

  def loadRules(): F[List[CategorizationRule]]

  def addRule(rule: CategorizationRule): F[Unit]

}

object RuleRepository {
  object syntax {
    def loadRules[F[_]](implicit repo: RuleRepository[F]): F[List[CategorizationRule]] = repo.loadRules()
    def addRule[F[_]](rule: CategorizationRule)(implicit repo: RuleRepository[F]): F[Unit] = repo.addRule(rule)
  }
}
