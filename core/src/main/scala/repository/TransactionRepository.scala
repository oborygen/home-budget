package repository

import java.util.UUID

import domain.{ProcessedTransaction, ThrowableMonadError}

trait TransactionRepository[F[_]] {

  def exists(id: String): F[Boolean]
  def isAllocated(id: String): F[Boolean]
  def load(id: String): F[Option[ProcessedTransaction]]
  def saveOrUpdate(transaction: ProcessedTransaction): F[Unit]
  def deleteBatch(batchId: UUID): F[Int]

}

object TransactionRepository {

  object syntax {

    def transactionExists[F[_]](id: String)(implicit repo: TransactionRepository[F]): F[Boolean] =
      repo.exists(id)

    def transactionIsAllocated[F[_]: ThrowableMonadError](id: String)(
        implicit repo: TransactionRepository[F]): F[Boolean] =
      repo.isAllocated(id)

    def saveTransaction[F[_]](transaction: ProcessedTransaction)(implicit repo: TransactionRepository[F]): F[Unit] =
      repo.saveOrUpdate(transaction)

    def deleteBatch[F[_]](batchId: UUID)(implicit repo: TransactionRepository[F]): F[Int] = repo.deleteBatch(batchId)
  }
}
