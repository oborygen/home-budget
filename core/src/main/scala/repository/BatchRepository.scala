package repository

import java.util.UUID

import domain.Batch

trait BatchRepository[F[_]] {

  def list: F[List[Batch]]

  def save(batch: Batch): F[Unit]

  def delete(batchId: UUID): F[Unit]

}

object BatchRepository {

  object syntax {

    def list[F[_]](implicit repo: BatchRepository[F]): F[List[Batch]] = repo.list

    def save[F[_]](batch: Batch)(implicit repo: BatchRepository[F]): F[Unit] = repo.save(batch)

    def delete[F[_]](batchId: UUID)(implicit repo: BatchRepository[F]): F[Unit] = repo.delete(batchId)

  }
}
