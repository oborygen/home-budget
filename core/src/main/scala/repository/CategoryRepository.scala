package repository

import java.util.UUID

import cats.implicits._
import domain.{Category, ThrowableMonadError}

trait CategoryRepository[F[_]] {
  def load(id: UUID): F[Option[Category]]
  def allCategories: F[List[Category]]
  def save(category: Category): F[Unit]
}

object CategoryRepository {

  object syntax {

    def loadCategory[F[_]](id: UUID)(implicit repo: CategoryRepository[F]): F[Option[Category]] = repo.load(id)

    def categoryExists[F[_]: ThrowableMonadError](id: UUID)(implicit repo: CategoryRepository[F]): F[Boolean] =
      repo.load(id).map(_.isDefined)

    def allCategories[F[_]](implicit repo: CategoryRepository[F]): F[List[Category]] = repo.allCategories

    def saveCategory[F[_]](category: Category)(implicit repo: CategoryRepository[F]): F[Unit] = repo.save(category)
  }
}
