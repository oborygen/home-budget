package domain

import java.time.LocalDate

import utest._

object PlanningIntervalTest extends TestSuite {
  override def tests: Tests = Tests {
    "monthly range" - {
      val range = Monthly.range(LocalDate.of(2016, 2, 10))
      assert(range == (LocalDate.of(2016, 2, 1), LocalDate.of(2016, 2, 29)))
    }
    "quarterly range" - {
      val range = Quarterly.range(LocalDate.of(2016, 2, 10))
      assert(range == (LocalDate.of(2016, 1, 1), LocalDate.of(2016, 3, 31)))
    }
    "weekly range" - {
      val range = Weekly.range(LocalDate.of(2016, 2, 29))
      assert(range == (LocalDate.of(2016, 2, 29), LocalDate.of(2016, 3, 6)))
    }
    "yearly range" - {
      val range = Yearly.range(LocalDate.of(2016, 2, 29))
      assert(range == (LocalDate.of(2016, 1, 1), LocalDate.of(2016, 12, 31)))
    }
  }
}
