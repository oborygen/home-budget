import java.time.{LocalDate, LocalDateTime}
import java.util.UUID

import domain.{Batch, ProcessedTransaction, SpendAllocation, SpendPlan}

package object repository {

  abstract class DummyAllocationRepository[F[_]] extends AllocationRepository[F] {
    override def loadLatest(planId: UUID, start: LocalDate, end: LocalDate): F[Option[SpendAllocation]] = ???
    override def save(allocation: SpendAllocation): F[Unit] = ???
    override def increaseAmount(
                                 spendPlanId: UUID,
                                 batchId: UUID,
                                 lastUpdate: LocalDateTime,
                                 intervalStart: LocalDate,
                                 intervalEnd: LocalDate,
                                 amount: BigDecimal): F[Unit] = ???
    override def deleteBatch(batchId: UUID): F[Int] = ???
  }


  abstract class DummySpendPlanRepository[F[_]] extends SpendPlanRepository[F] {
    override def load(id: UUID): F[Option[SpendPlan]] = ???
    override def allPlans: F[Set[SpendPlan]] = ???
    override def save(spendPlan: SpendPlan): F[Unit] = ???
  }

  abstract class DummyTransactionRepository[F[_]] extends TransactionRepository[F] {
    override def exists(id: String): F[Boolean] = ???
    override def isAllocated(id: String): F[Boolean] = ???
    override def saveOrUpdate(transaction: ProcessedTransaction): F[Unit] = ???
    override def load(id: String): F[Option[ProcessedTransaction]] = ???
    override def deleteBatch(batchId: UUID): F[Int] = ???
  }

  abstract class DummyBatchRepository[F[_]] extends BatchRepository[F] {
    override def list: F[List[Batch]] = ???
    override def save(batch: Batch): F[Unit] = ???
    override def delete(batchId: UUID): F[Unit] = ???
  }
}
