package finance.domain

import java.time.{LocalDate, ZonedDateTime}

import com.fortysevendeg.scalacheck.datetime.jdk8.ArbitraryJdk8._
import org.scalacheck._
import Gen._
import Arbitrary.arbitrary
import domain.Transaction

object TransactionBuilder {

  val genTransaction: Gen[Transaction] = for {
    id <- arbitrary[String]
    title <- arbitrary[String]
    counterparty <- arbitrary[String]
    transactionDate <- arbitrary[ZonedDateTime]
    bookingDate <- arbitrary[ZonedDateTime]
    amount <- arbitrary[BigDecimal]
  } yield {
    Transaction(id,
                transactionDate.toLocalDate,
                bookingDate.toLocalDate,
                title,
                counterparty,
                amount)
  }

  def withTitleFragment(t: String): Gen[Transaction] =
    for {
      id <- arbitrary[String]
      titlePrefix <- arbitrary[String]
      titleSuffix <- arbitrary[String]
      counterparty <- arbitrary[String]
      transactionDate <- arbitrary[ZonedDateTime]
      bookingDate <- arbitrary[ZonedDateTime]
      amount <- arbitrary[BigDecimal]
    } yield {
      Transaction(id,
                  transactionDate.toLocalDate,
                  bookingDate.toLocalDate,
                  titlePrefix + t + titleSuffix,
                  counterparty,
                  amount)
    }

}
