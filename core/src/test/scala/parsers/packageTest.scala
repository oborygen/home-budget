package parsers

import java.text.{DecimalFormat, DecimalFormatSymbols, NumberFormat}
import java.time.LocalDate
import java.time.format.DateTimeFormatter

import utest._
import cats.implicits._

object packageTest extends TestSuite {

  implicit val ctx: ParsingContext = {
    ParsingContext(
      decimalFormatInstance(','), DateTimeFormatter.ISO_DATE
    )
  }

  override def tests: Tests = Tests {

    "Decimal format instance should parse a number" - {
      val numberParser = decimalFormatInstance(',')
      assert(numberParser("1234,45").contains(BigDecimal("1234.45")))
      assert(numberParser("1234.45").isEmpty)
      assert(numberParser("1.234,45").isEmpty)
    }

    "validate required BigDecimal" - {
      assert(Some("-104,43").required[BigDecimal] == BigDecimal("-104.43").validNel[String])
      assert(Some("-1234,43").required[BigDecimal] == BigDecimal("-1234.43").validNel[String])
      assert(Some("666777,888").required[BigDecimal] == BigDecimal("666777.888").validNel[String])
      assert(Some("1.1212").required[BigDecimal] == "Invalid number 1.1212".invalidNel[BigDecimal])
      assert(Some("").required[BigDecimal] == "Empty string for number".invalidNel[BigDecimal])
    }

    "validate required Date" - {
      assert(Some("2018-10-11").required[LocalDate] == LocalDate.of(2018, 10, 11).validNel[String])
      assert(Some("").required[LocalDate] == "Empty string for date".invalidNel[LocalDate])
    }

    "validate required Int" - {
      val source = Some("12")
      val result = source.required[Int]
      assert(result.isValid)
      assert(result == 12.validNel[String])
    }

    "validate optional String" - {
      val source = None
      val result = source.optional[String]
      assert(result.isValid)
      assert(result == None.validNel[String])
    }
  }
}
