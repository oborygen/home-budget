package parsers

import utest._
import StringOps._

object StringOpsTest extends TestSuite {
  override def tests: Tests = Tests {

    "remove quotes" - {
      assert(removeQuotes("\"abc\"") == "abc")
      assert(removeQuotes("\"abc") == "abc")
      assert(removeQuotes("abc\"") == "abc")
      assert(removeQuotes("abc") == "abc")
    }

    "split line" - {
      assert(split("abc;123;;3d;", ';') == Seq("abc", "123", "", "3d", ""))
      assert(split(";;;", ';') == Seq.fill(4)(""))
    }

  }
}
