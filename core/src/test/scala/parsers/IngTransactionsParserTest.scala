package parsers

import utest._

import scala.io.Source

object IngTransactionsParserTest extends TestSuite {

  override def tests: Tests = Tests {
    "parse a csv file" - {
      val source = Source.fromResource("transakcje-ing.csv")(IngTransactionsParser.defaultEncoding)
      val lines = source.getLines().toStream
      val transactions = IngTransactionsParser.parse(lines)
      assert(transactions.size == 162)
    }
  }

}
