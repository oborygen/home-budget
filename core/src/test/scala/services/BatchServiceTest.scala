package services

import java.time.{LocalDate, LocalDateTime}
import java.util.UUID

import cats.implicits._
import domain._
import repository._
import utest._

import scala.util.{Random, Success, Try}

object BatchServiceTest extends TestSuite {

  def transaction(id: String, date: LocalDate, title: String, value: BigDecimal) =
    Transaction(id, date, date, title, "", value)

  val Stream(shoesCategory, lunchCategory, coffeeCategory, aCategory, bCategory, otherCategory) =
    Stream.continually(UUID.randomUUID()).take(6)
  val Stream(shoesPlan, lunchPlan, coffeePlan, abPlan) = Stream.continually(UUID.randomUUID()).take(4)

  val categories: PartialFunction[Transaction, UUID] = {
    case Transaction(_, _, _, "shoes", _, _)  => shoesCategory
    case Transaction(_, _, _, "lunch", _, _)  => lunchCategory
    case Transaction(_, _, _, "coffee", _, _) => coffeeCategory
    case Transaction(_, _, _, "a", _, _)      => aCategory
    case Transaction(_, _, _, "b", _, _)      => bCategory
    case Transaction(_, _, _, "other", _, _)  => otherCategory
  }

  implicit val spendPlanRepository: DummySpendPlanRepository[Try] = new DummySpendPlanRepository[Try] {
    override def allPlans: Try[Set[SpendPlan]] = Success(
      Set(
        SpendPlan(shoesPlan, Set(shoesCategory), LocalDate.now, Monthly, 1000),
        SpendPlan(lunchPlan, Set(lunchCategory), LocalDate.now, Monthly, 1000),
        SpendPlan(coffeePlan, Set(coffeeCategory), LocalDate.now, Monthly, 1000),
        SpendPlan(abPlan, Set(aCategory, bCategory), LocalDate.now, Monthly, 1000)
      )
    )
  }

  val initialAllocationAmount: BigDecimal = Random.nextInt(100)

  implicit val allocationRepository: DummyAllocationRepository[Try] = new DummyAllocationRepository[Try] {
    override def loadLatest(planId: UUID, start: LocalDate, end: LocalDate): Try[Option[SpendAllocation]] = Success(
      Some(SpendAllocation(UUID.randomUUID, planId, None, LocalDateTime.now, start, end, initialAllocationAmount))
    )
  }

  implicit val transactionRepository: DummyTransactionRepository[Try] = new DummyTransactionRepository[Try] {
    override def isAllocated(id: String): Try[Boolean] = Success(id.startsWith("exists"))
  }

  implicit val batchRepository: DummyBatchRepository[Try] = new DummyBatchRepository[Try] {}

  def getAmount(allocations: List[(SpendAllocation, List[ProcessedTransaction])], planId: UUID): Option[BigDecimal] =
    allocations.collectFirst { case (a, _) if a.spendPlanId == planId => a.amount }

  def getAmounts(
      allocations: List[(SpendAllocation, List[ProcessedTransaction])],
      planId: UUID): List[(LocalDate, BigDecimal)] =
    allocations.collect { case (a, _) if a.spendPlanId == planId => a.intervalStart -> a.amount }

  override def tests: Tests = Tests {
    "allocate new transactions" - {
      val transactions = Seq(
        transaction("1", LocalDate.parse("2018-12-15"), "shoes", 100),
        transaction("2", LocalDate.parse("2018-12-16"), "lunch", 10),
        transaction("3", LocalDate.parse("2018-12-17"), "coffee", 50),
        transaction("4", LocalDate.parse("2018-12-18"), "lunch", 20),
        transaction("5", LocalDate.parse("2018-12-18"), "coffee", 5),
        transaction("6", LocalDate.parse("2018-12-18"), "a", 5),
        transaction("7", LocalDate.parse("2018-12-18"), "b", 5),
        transaction("8", LocalDate.parse("2018-12-18"), "cakes", 666), // cannot be categorized
        transaction("10", LocalDate.parse("2018-12-18"), "other", 100), // cannot be allocated
        transaction("14", LocalDate.parse("2019-01-10"), "lunch", 15), //different date
        transaction("exists1", LocalDate.parse("2018-12-18"), "coffee", 500) //should be filtered
      )

      val service = new BatchService[Try](categories.lift)
      val resultT: Try[BatchResult] = service.processBatch(transactions)

      assert(resultT.isSuccess)
      resultT.foreach { result =>
        import result._
        assert(
          allocations.size == 5,
          getAmount(allocations, shoesPlan) == (initialAllocationAmount + 100).some,
          getAmount(allocations, coffeePlan) == (initialAllocationAmount + 55).some,
          getAmount(allocations, abPlan) == (initialAllocationAmount + 10).some,
          getAmounts(allocations, lunchPlan).contains(LocalDate.parse("2018-12-01") -> (initialAllocationAmount + 30)),
          getAmounts(allocations, lunchPlan).contains(LocalDate.parse("2019-01-01") -> (initialAllocationAmount + 15)),
          existing.size == 1,
          existing.head.id == "exists1",
          uncategorized.size == 1,
          unallocated.size == 1,
          unallocated.head.transaction.id == "10",
          uncategorized.head.transaction.id == "8"
        )
      }
    }
  }
}
