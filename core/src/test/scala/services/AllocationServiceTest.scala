package services

import java.time.{LocalDate, LocalDateTime}
import java.util.UUID

import cats.implicits._
import domain.{Monthly, SpendAllocation}
import repository.{AllocationRepository, DummyAllocationRepository}
import utest._

import scala.util.{Success, Try}

object AllocationServiceTest extends TestSuite {

  override def tests: Tests = Tests {
    "load allocation if exists" - {
      val planId = UUID.randomUUID()
      val transactionDate = LocalDate.now
      val (start, end) = Monthly.range(transactionDate)

      val allocation = SpendAllocation(UUID.randomUUID, planId, None, LocalDateTime.now, start, end, 10)

      implicit val allocationRepository: AllocationRepository[Try] = new DummyAllocationRepository[Try] {
        override def loadLatest(planId: UUID, start: LocalDate, end: LocalDate): Try[Option[SpendAllocation]] =
          Success(Some(allocation))
      }

      val result = AllocationApi.findAllocation(planId, start, end)

      assert(result.get == allocation)
    }

    "create allocation if none exists" - {
      val planId = UUID.randomUUID()
      val transactionDate = LocalDate.now
      val (start, end) = Monthly.range(transactionDate)

      implicit val allocationRepository: AllocationRepository[Try] = new DummyAllocationRepository[Try] {
        override def loadLatest(planId: UUID, start: LocalDate, end: LocalDate): Try[Option[SpendAllocation]] =
          Success(None)
      }

      val result = AllocationApi.findAllocation(planId, start, end)

      assert(result.isSuccess)
      result.foreach { allocation =>
        assert(allocation.intervalStart == start)
        assert(allocation.intervalEnd == end)
        assert(allocation.amount == 0)
        assert(allocation.spendPlanId == planId)
      }
    }

  }
}
