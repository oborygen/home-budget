package services

import java.util.UUID

import cats.implicits._
import domain.CategorizationRule
import repository.RuleRepository
import utest._

import scala.util.Try

object CategorizationRuleServiceTest extends TestSuite {

  val shoesCategory: UUID = UUID.randomUUID()
  val booksCategory: UUID = UUID.randomUUID()

  override def tests: Tests = Tests {
    "load all rules" - {
      implicit val ruleRepository = new RuleRepository[Try] {
        override def loadRules(): Try[List[CategorizationRule]] = Try {
          List(
            CategorizationRule(UUID.randomUUID(), "title like '*shoes*'", shoesCategory),
            CategorizationRule(UUID.randomUUID(), "counterparty like '*amazon*'", booksCategory)
          )
        }

        override def addRule(rule: CategorizationRule): Try[Unit] = ???
      }
      val rules = CategorizationRuleService.loadRules()
      assert(rules.isSuccess)
      assert(rules.get.size == 2)
    }

    "skip invalid rule" - {
      implicit val ruleRepository = new RuleRepository[Try] {
        override def loadRules(): Try[List[CategorizationRule]] = Try {
          List(
            CategorizationRule(UUID.randomUUID(), "title like '*shoes*'", shoesCategory),
            CategorizationRule(UUID.randomUUID(), "fakefield like '*amazon*'", booksCategory),
            CategorizationRule(UUID.randomUUID(), "title fakefunction '*amazon*'", booksCategory)
          )
        }

        override def addRule(rule: CategorizationRule): Try[Unit] = ???
      }
      val rules = CategorizationRuleService.loadRules()
      assert(rules.isSuccess)
      assert(rules.get.size == 1)
    }
  }
}
