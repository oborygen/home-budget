package rules

import java.time.LocalDate

import domain.Transaction
import rules.Algebra.{Contains, Like, Or, StringField}
import utest._

object AlgebraTest extends TestSuite {
  override def tests: Tests = Tests {
    "Or and Contains" - {
      val operation = Or(Contains(StringField("title"), "trening"), Contains(StringField("title"), "other"))
      val transaction = Transaction("1", LocalDate.now, LocalDate.now, "Trening z Beatą", "rehafit", 100)
      assert(operation.eval(transaction))
    }

    "Like operation" - {
      val operation = Like(StringField("title"), "*trening*")
      val transaction = Transaction("1", LocalDate.now, LocalDate.now, "za Trening z Beatą", "rehafit", 100)
      assert(operation.eval(transaction))
    }

    "Like operation not matching" - {
      val operation = Like(StringField("title"), "*trening*")
      val transaction = Transaction("1", LocalDate.now, LocalDate.now, "paluszki i soda", "rabat", 100)
      assert(!operation.eval(transaction))
    }
  }
}
