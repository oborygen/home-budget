package rules

import java.time.LocalDate
import java.util.UUID

import domain.Transaction
import utest._

object CategorizerTest extends TestSuite {


  override def tests: Tests = Tests {

    val alco = UUID.randomUUID()
    val sport = UUID.randomUUID()

    "categorize" - {
      val rules: Seq[Rule] = Seq(
        { case Transaction(_, _, _, "Trening", _, _) => sport },
        { case Transaction(_, _, _, _, "lokietka", _) => alco }
      )
      val categorizer = new Categorizer(_ => rules)
      val res1 = categorizer.categorize(Transaction("12345", LocalDate.now, LocalDate.now, "test", "lokietka", 1.0))
      val res2 = categorizer.categorize(Transaction("2", LocalDate.now, LocalDate.now, "Trening", "rehafit", 1.0))
      val res3 = categorizer.categorize(Transaction("2", LocalDate.now, LocalDate.now, "sciema", "sciema", 1.0))
      assert(res1.contains(alco))
      assert(res2.contains(sport))
      assert(res3.isEmpty)
    }
  }
}
