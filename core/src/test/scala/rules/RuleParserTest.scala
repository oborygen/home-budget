package rules

import rules.Algebra.{Contains, Or, StringField}
import utest._

import scala.util.Success

object RuleParserTest extends TestSuite {
  override def tests: Tests = Tests {
    "parse field identifier" - {
      val parser = new RuleParser("title")
      val rule = parser.StringFieldRule.run()
      assert(rule.isSuccess)
    }

    "error parsing field identifier" - {
      val parser = new RuleParser("fake")
      val rule = parser.StringFieldRule.run()
      assert(rule.isFailure)
    }

    "parse text" - {
      val parser = new RuleParser("\"some text -+ ąś!\"")
      val rule = parser.Text.run()
      assert(rule.isSuccess)
      assert(rule == Success("some text -+ ąś!"))
    }

    "parse text with single quotes" - {
      val parser = new RuleParser("'some text -+ ąś!'")
      val rule = parser.Text.run()
      assert(rule.isSuccess)
      assert(rule == Success("some text -+ ąś!"))
    }

    "parse contains operator" - {
      val parser = new RuleParser("title contains \"some text\"")
      val rule = parser.ContainsRule.run()
      assert(rule.isSuccess)
      assert(rule == Success(Contains(StringField("title"), "some text")))
    }

    "parse complex expression" - {
      val parser = new RuleParser("title contains 'some text' OR title contains \"other\"")
      val rule = parser.InputLine.run()
      assert(rule.isSuccess)
      assert(rule == Success(Or(Contains(StringField("title"), "some text"), Contains(StringField("title"),"other"))))
    }


  }
}
