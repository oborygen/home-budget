import sbt._
import sbtrelease.ReleaseStateTransformations._
import Dependencies._

ThisBuild / organization := "com.oborygen"

ThisBuild / name := "home-budget"

ThisBuild / scalaVersion := "2.12.8"

lazy val commonSettings = Seq(
  scalacOptions ++= Seq(
    "-Ypartial-unification",
    "-language:higherKinds",
    "-language:implicitConversions",
    "-target:jvm-1.8",
    "-language:postfixOps",
    "-language:implicitConversions",
    "-language:reflectiveCalls",
    "-language:higherKinds",
    "-language:existentials",
    "-Xfatal-warnings",
    "-deprecation"
  ),
  testFrameworks += new TestFramework("utest.runner.Framework"),
  resolvers += Resolver.sonatypeRepo("releases"),
  libraryDependencies ++= commonDependencies ++ testDependencies
)

lazy val core = (project in file("core"))
  .settings(
    commonSettings,
    Defaults.itSettings,
    libraryDependencies ++= commonDependencies ++ coreDependencies
  )
  .configs(IntegrationTest)


lazy val client = (project in file("client"))
  .dependsOn(core, postgres)
  .enablePlugins(BuildInfoPlugin)
  .settings(
    commonSettings,
    addCompilerPlugin("org.spire-math" %% "kind-projector" % "0.9.8"),
    libraryDependencies ++= terminalDependencies,
    buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion),
    test in assembly := {},
    mainClass in assembly := Some("boot.Start"),
    releaseIgnoreUntrackedFiles := true,
    releaseTagName := s"stable/${if (releaseUseGlobalVersion.value) (version in ThisBuild).value else version.value}",
    releaseProcess := Seq[ReleaseStep](
      checkSnapshotDependencies,              // : ReleaseStep
      inquireVersions,                        // : ReleaseStep
      runClean,                               // : ReleaseStep
      runTest,                                // : ReleaseStep
      setReleaseVersion,                      // : ReleaseStep
      commitReleaseVersion,                   // : ReleaseStep, performs the initial git checks
      tagRelease,                             // : ReleaseStep
      setNextVersion,                         // : ReleaseStep
      commitNextVersion,                      // : ReleaseStep
      pushChanges                             // : ReleaseStep, also checks that an upstream branch is properly configured
    )
  )
  .configs(IntegrationTest)

lazy val postgres = (project in file("postgres"))
  .dependsOn(core)
  .settings(
    commonSettings,
    Defaults.itSettings,
    libraryDependencies ++= postgresDependencies ++ itDependencies
  )
  .configs(IntegrationTest)

lazy val homebudget = (project in file("."))
  .aggregate(core, postgres, client)
