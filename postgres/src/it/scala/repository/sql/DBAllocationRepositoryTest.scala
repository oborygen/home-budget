package repository.sql

import java.time.{LocalDate, LocalDateTime}
import java.util.UUID

import cats.implicits._
import domain.{Monthly, SpendAllocation, SpendPlan}
import doobie.implicits._
import utest._

object DBAllocationRepositoryTest extends TestSuite with PostgresTests {

  val repo = new DBAllocationRepository
  val spendPlanRepo = new DBSpendPlanRepository
  val spendPlan = SpendPlan(UUID.randomUUID(), Set(UUID.randomUUID), LocalDate.now, Monthly, 1000)

  val initialize: doobie.ConnectionIO[Unit] =
    List(
      spendPlanRepo.initialize,
      repo.initialize,
      spendPlanRepo.save(spendPlan)
    ).sequence.map(_ => ())

  override def tests: Tests = Tests {

    "load latest allocation from file" - {
      val allocation1 = SpendAllocation(
        UUID.randomUUID(),
        spendPlan.id,
        Some(UUID.randomUUID()),
        LocalDateTime.now(),
        LocalDate.parse("2018-12-01"),
        LocalDate.parse("2018-12-31"),
        10.0
      )
      val allocation2 =
        allocation1.copy(
          id = UUID.randomUUID,
          batchId = Some(UUID.randomUUID),
          amount = 100,
          lastUpdate = allocation1.lastUpdate.plusDays(1))
      val allocation3 =
        allocation1.copy(
          id = UUID.randomUUID,
          batchId = Some(UUID.randomUUID),
          amount = 200,
          lastUpdate = allocation1.lastUpdate.plusDays(2))

      run { xa =>
        (for {
          _ <- repo.save(allocation1)
          _ <- repo.save(allocation2)
          _ <- repo.save(allocation3)
          allocation <- repo
            .loadLatest(allocation1.spendPlanId, allocation1.intervalStart, allocation1.intervalEnd)
        } yield {
          assert(allocation.isDefined)
          assert(allocation.get.spendPlanId == allocation3.spendPlanId)
          assert(allocation.get.id == allocation3.id)
          assert(allocation.get.amount == allocation3.amount)
        }).transact(xa)
      }
    }

    "doesn't return allocation when file doesn't exist" - {
      run { xa =>
        (for {
          allocation <- repo
            .loadLatest(
              UUID.fromString("DB518861-779F-4862-B567-391A21F23441"),
              LocalDate.parse("2018-12-01"),
              LocalDate.parse("2018-12-31"))
        } yield {
          assert(allocation.isEmpty)
        }).transact(xa)
      }
    }

    "write allocation" - {
      val allocation = SpendAllocation(
        UUID.randomUUID(),
        spendPlan.id,
        Some(UUID.randomUUID()),
        LocalDateTime.now(),
        LocalDate.parse("2018-12-01"),
        LocalDate.parse("2018-12-31"),
        123.0
      )

      run { xa =>
        (for {
          _ <- repo.save(allocation)
          result <- repo
            .loadLatest(allocation.spendPlanId, allocation.intervalStart, allocation.intervalEnd)
        } yield {
          assert(result.isDefined)
        }).transact(xa)
      }
    }

    "create allocation" - {
      val batchId = UUID.randomUUID
      val start = LocalDate.parse("2018-12-01")
      val end = LocalDate.parse("2018-12-31")

      val amountToIncrease = 10.0
      run { xa =>
        (for {
          _ <- repo.increaseAmount(spendPlan.id, batchId, LocalDateTime.now, start, end, amountToIncrease)
          result <- repo
            .loadLatest(spendPlan.id, start, end)
        } yield {
          assert(result.isDefined)
          assert(result.get.amount == 10.0)
        }).transact(xa)
      }
    }

    "increase allocation" - {
      val allocation = SpendAllocation(
        UUID.randomUUID(),
        spendPlan.id,
        Some(UUID.randomUUID()),
        LocalDateTime.now(),
        LocalDate.parse("2018-12-01"),
        LocalDate.parse("2018-12-31"),
        123.0
      )
      val amountToIncrease = 10.0

      run { xa =>
        (for {
          _ <- repo.save(allocation)
          _ <- repo.increaseAmount(
            allocation.spendPlanId,
            allocation.batchId.get,
            LocalDateTime.now,
            allocation.intervalStart,
            allocation.intervalEnd,
            amountToIncrease)
          result <- repo
            .loadLatest(allocation.spendPlanId, allocation.intervalStart, allocation.intervalEnd)
        } yield {
          assert(result.isDefined)
          assert(result.get.amount == 133.0)
        }).transact(xa)
      }
    }

  }

}
