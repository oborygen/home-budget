package repository.sql

import java.time.{LocalDate, LocalDateTime}
import java.util.UUID

import cats.implicits._
import domain.{Batch, Category, ProcessedTransaction, Transaction}
import doobie.implicits._
import utest._

object DBTransactionRepositoryTest extends TestSuite with PostgresTests {

  private val repo = new DBTransactionRepository()
  private val batchRepo = new DBBatchRepository
  private val categoriesRepo = new DBCategoryRepository

  private val fooCategory = Category(UUID.randomUUID, "foo", None)
  private val batch = Batch(UUID.randomUUID, LocalDateTime.now, 0, 0, 0, 0, 0)

  override def initialize: doobie.ConnectionIO[Unit] =
    List(
      categoriesRepo.initialize,
      batchRepo.initialize,
      repo.initialize,
      categoriesRepo.save(fooCategory),
      batchRepo.save(batch)
    ).sequence.map(_ => ())

  override def tests: Tests = Tests {
    "save transaction and verify it exists" - {
      val transaction = ProcessedTransaction(
        Transaction("1", LocalDate.now, LocalDate.now, "Test", "shop A", 11.10),
        fooCategory.id.some,
        batch.id)
      run { xa =>
        (for {
          _ <- repo.saveOrUpdate(transaction)
          result <- repo.exists(transaction.transaction.id)
        } yield {
          assert(result)
        }).transact(xa)
      }
    }

    "update transaction and verify it exists" - {
      val repo = new DBTransactionRepository()
      val transactionId = "2"
      val transaction1 = ProcessedTransaction(
        Transaction(transactionId, LocalDate.now, LocalDate.now, "Test", "shop A", 11.10),
        None,
        batch.id)
      val transaction12 = ProcessedTransaction(
        Transaction(transactionId, LocalDate.now, LocalDate.now, "Test", "shop A", 100),
        fooCategory.id.some,
        batch.id)
      run { xa =>
        (for {
          _ <- repo.saveOrUpdate(transaction1)
          result1 <- repo.load(transactionId)
          _ <- repo.saveOrUpdate(transaction12)
          result2 <- repo.load(transactionId)
        } yield {
          assert(result1.get.categoryId.isEmpty)
          assert(result2.get.categoryId == transaction12.categoryId)
        }).transact(xa)
      }
    }

  }
}
