package repository.sql

import java.time.LocalDate
import java.util.UUID

import cats.effect.IO
import cats.implicits._
import domain.{Category, Monthly, SpendPlan}
import doobie.implicits._
import utest._

object DBSpendPlanRepositoryTest extends TestSuite with PostgresTests {

  private val repo = new DBSpendPlanRepository
  private val categoriesRepo = new DBCategoryRepository

  private val food = UUID.randomUUID()
  private val clothes = UUID.randomUUID()
  private val alco = UUID.randomUUID()
  private val foodCategory = Category(food, "Food", None)
  private val clothesCategory = Category(clothes, "Clothes", None)
  private val alcoholCategory = Category(alco, "Alcohol", None)


  override def initialize: doobie.ConnectionIO[Unit] = List(
    categoriesRepo.initialize,
    repo.initialize,
    categoriesRepo.save(foodCategory),
    categoriesRepo.save(clothesCategory),
    categoriesRepo.save(alcoholCategory)
  ).sequence.map(_ => ())


  override def tests: Tests = Tests {
    "save spend plan" - {
      val spendPlan1 = SpendPlan(UUID.randomUUID(), Set(food, clothes), LocalDate.now, Monthly, 1000)
      val spendPlan2 = SpendPlan(UUID.randomUUID(), Set(alco), LocalDate.now, Monthly, 2000)

      val repo = new DBSpendPlanRepository
      run{ xa =>
        (for {
          _ <- repo.save(spendPlan1)
          _ <- repo.save(spendPlan2)
          foodPlans <- repo.load(spendPlan1.id)
          alcoPlans <- repo.load(spendPlan2.id)
        } yield {
          assert(foodPlans.exists(_.id == spendPlan1.id))
          assert(alcoPlans.exists(_.id == spendPlan2.id))
        }).transact(xa)
      }
    }

    "save spend plan should fail when there is no such category" - {
      val food = UUID.randomUUID()
      val spendPlan = SpendPlan(UUID.randomUUID(), Set(food, UUID.randomUUID), LocalDate.now, Monthly, 1000)

      val repo = new DBSpendPlanRepository
      run{ xa =>
        (for {
          _ <- repo.save(spendPlan)
        } yield {
          assert(false)
        }).transact(xa).handleError {
          case e if e.getMessage == "Undefined categories" =>
            assert(true)
        }
      }
    }

    "load all plans" - {
      val spendPlan1 = SpendPlan(UUID.randomUUID(), Set(food, clothes), LocalDate.now, Monthly, 1000)
      val spendPlan2 = SpendPlan(UUID.randomUUID(), Set(alco), LocalDate.now, Monthly, 2000)

      run { xa =>
        (for {
          _ <- repo.save(spendPlan1)
          _ <- repo.save(spendPlan2)
          records <- repo.allPlans
        } yield {
          assert(records.exists(_.id == spendPlan1.id))
          assert(records.exists(_.id == spendPlan2.id))
        }).transact(xa)
      }
    }
  }
}
