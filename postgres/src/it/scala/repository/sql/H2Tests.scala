package repository.sql

import cats.effect._
import cats.implicits._
import doobie._
import doobie.implicits._
import doobie.h2._

import scala.concurrent.ExecutionContext

trait H2Tests {

  private implicit val cs = IO.contextShift(ExecutionContext.global)

  lazy val transactor: Resource[IO, H2Transactor[IO]] =
    for {
      ce <- ExecutionContexts.fixedThreadPool[IO](32) // our connect EC
      te <- ExecutionContexts.cachedThreadPool[IO]    // our transaction EC
      xa <- H2Transactor.newH2Transactor[IO](
        "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1", // connect URL
        "sa",                                   // username
        "",                                     // password
        ce,                                     // await connection here
        te                                      // execute JDBC operations here
      )
    } yield xa

  def run(initialize: ConnectionIO[Unit])(test: Transactor[IO] => IO[Unit]) = {
    transactor.use { xa =>
      for {
        _ <- initialize.transact(xa)
        _ <- test(xa)
      } yield {}
    }.unsafeRunSync()
  }
}
