package repository.sql

import cats.effect._
import cats.implicits._
import doobie._
import doobie.implicits._
import doobie.h2._
import org.testcontainers.containers.PostgreSQLContainer
import utest.TestSuite

import scala.concurrent.{ExecutionContext, Future}

trait PostgresTests { self: TestSuite =>

  private val container = new PostgreSQLContainer()

  private implicit val cs = IO.contextShift(ExecutionContext.global)

  override def utestAfterAll(): Unit = container.stop()

  def initialize: ConnectionIO[Unit]

  lazy val transactor: Transactor[IO] = {
    container.start()
    val tx = Transactor.fromDriverManager[IO](
      container.getDriverClassName,
      container.getJdbcUrl, // connect URL
      container.getUsername, // username
      container.getPassword
    )

    initialize.transact(tx).unsafeRunSync()
    tx
  }

  def run(test: Transactor[IO] => IO[Unit]): Future[Unit] = {
    test(transactor).unsafeToFuture()
  }

}
