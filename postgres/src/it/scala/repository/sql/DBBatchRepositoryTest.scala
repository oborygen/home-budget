package repository.sql

import java.time.LocalDateTime
import java.util.UUID

import domain.Batch
import doobie.free.connection.ConnectionIO
import doobie.implicits._
import utest._

object DBBatchRepositoryTest extends TestSuite with PostgresTests {

  private val repo = new DBBatchRepository

  override val initialize: ConnectionIO[Unit] = repo.initialize

  override def tests: Tests = Tests {
    "save batch" - {
      val id = UUID.randomUUID()
      val batch = Batch(id, LocalDateTime.now, 100, 5, 90, 20, 90)
      run { xa =>
        (for {
          _ <- repo.save(batch)
          batches <- repo.list
        } yield {
          assert(batches.size == 1)
        }).transact(xa)
      }
    }

    "delete batch" - {
      val id = UUID.randomUUID()
      val batch = Batch(id, LocalDateTime.now, 100, 5, 90, 20, 90)
      run { xa =>
        (for {
          _ <- repo.save(batch)
          _ <- repo.delete(id)
          batches <- repo.list
        } yield {
          assert(!batches.exists(_.id == id))
        }).transact(xa)
      }
    }
  }
}
