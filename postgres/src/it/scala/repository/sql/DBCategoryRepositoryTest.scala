package repository.sql

import java.util.UUID

import cats.implicits._
import domain.Category
import doobie.implicits._
import utest._

object DBCategoryRepositoryTest extends TestSuite with PostgresTests {

  val repo = new DBCategoryRepository

  override def initialize: doobie.ConnectionIO[Unit] = repo.initialize

  override def tests: Tests = Tests {
    "save category" - {
      val category1 = Category(UUID.randomUUID, "test1", None)
      val category2 = Category(UUID.randomUUID, "test2", category1.id.some)
      run { xa =>
        (for {
          _ <- repo.save(category1)
          _ <- repo.save(category2)
          categories <- repo.allCategories
        } yield {
          assert(categories.contains(category1))
          assert(categories.contains(category2))
        }).transact(xa)

      }

    }

    "update category" - {
      val category = Category(UUID.randomUUID, "test1", None)
      run { xa =>
        (for {
          _ <- repo.save(category)
          _ <- repo.save(category.copy(name = "test2"))
          category <- repo.load(category.id)
        } yield {
          assert(category.get.name == "test2")
        }).transact(xa)

      }

    }

  }
}
