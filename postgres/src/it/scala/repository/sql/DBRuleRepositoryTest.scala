package repository.sql

import java.util.UUID

import cats.implicits._
import domain.{CategorizationRule, Category}
import doobie.implicits._
import utest._

object DBRuleRepositoryTest extends TestSuite with PostgresTests {

  private val fooCategory = Category(UUID.randomUUID, "foo", None)
  private val rule1: CategorizationRule = CategorizationRule(UUID.randomUUID(), "title like '*shoes*'", fooCategory.id)
  private val rule2: CategorizationRule = CategorizationRule(UUID.randomUUID(), "title like '*flowers*'", fooCategory.id)
  private val repo = new DBRuleRepository

  private val categoriesRepo = new DBCategoryRepository

  override def initialize: doobie.ConnectionIO[Unit] = List(
    categoriesRepo.initialize,
    repo.initialize,
    categoriesRepo.save(fooCategory)
  ).sequence.map(_ => ())


  override def tests: Tests = Tests {
    "save and load rules" - {
      run { xa =>
        (for {
         _ <- repo.addRule(rule1)
         _ <- repo.addRule(rule2)
         rules <- repo.loadRules()
       } yield {
         assert(rules.size == 2)
         assert(rules.contains(rule1))
         assert(rules.contains(rule2))
       }).transact(xa)
      }
    }
  }
}
