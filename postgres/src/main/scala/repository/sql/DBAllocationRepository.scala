package repository.sql

import java.time.{LocalDate, LocalDateTime}
import java.util.UUID

import domain.SpendAllocation
import cats.implicits._
import doobie._
import doobie.implicits._
import repository.AllocationRepository

class DBAllocationRepository
  extends AllocationRepository[ConnectionIO] {

  implicit private val readAllocation: Read[SpendAllocation] =
    Read[(UUID, UUID, UUID, LocalDateTime, LocalDate, LocalDate, BigDecimal)].map {
      case (id, spendPlanId, batchId, lastUpdate, start, end, amount) =>
        SpendAllocation(id, spendPlanId, Option(batchId), lastUpdate, start, end, amount)
    }

  override def loadLatest(planId: UUID, start: LocalDate, end: LocalDate): ConnectionIO[Option[SpendAllocation]] =
    sql"""
          select *
          from SPEND_ALLOCATIONS
          where spend_plan_id = $planId and interval_start = $start and interval_end = $end
          order by last_update desc
          limit 1
    """.query[SpendAllocation].option

  override def save(allocation: SpendAllocation): ConnectionIO[Unit] =
    sql"""
          insert into SPEND_ALLOCATIONS(id, spend_plan_id, batch_id, last_update, interval_start, interval_end, amount)
          values(${allocation.id}, ${allocation.spendPlanId}, ${allocation.batchId}, ${LocalDateTime.now},
                 ${allocation.intervalStart}, ${allocation.intervalEnd}, ${allocation.amount})
    """.update.run.map(_ => ())

  override def increaseAmount(
      spendPlanId: UUID,
      batchId: UUID,
      lastUpdate: LocalDateTime,
      intervalStart: LocalDate,
      intervalEnd: LocalDate,
      amount: BigDecimal): ConnectionIO[Unit] =
    for {
      updated <-sql"""
               update SPEND_ALLOCATIONS set amount=amount+$amount, last_update=${LocalDateTime.now}
               where batch_id=$batchId and spend_plan_id=$spendPlanId and interval_start=$intervalStart and interval_end=$intervalEnd
               """.update.run
      _ <- if (updated == 0) {
        sql"""
               insert into SPEND_ALLOCATIONS(id, spend_plan_id, batch_id, last_update, interval_start, interval_end, amount)
               values(${UUID.randomUUID}, ${spendPlanId}, ${batchId}, ${LocalDateTime.now},
               ${intervalStart}, ${intervalEnd}, ${amount})
            """.update.run
      } else {
        sql"""0""".pure[ConnectionIO]
      }
    } yield {
      ()
    }

  override def deleteBatch(batchId: UUID): doobie.ConnectionIO[Int] =
    sql"""delete from SPEND_ALLOCATIONS where batch_id = $batchId""".update.run

  def initialize: ConnectionIO[Unit] =
    List(
      sql"""
          CREATE TABLE IF NOT EXISTS SPEND_ALLOCATIONS(
            id VARCHAR PRIMARY KEY,
            spend_plan_id VARCHAR REFERENCES SPEND_PLANS,
            batch_id VARCHAR NOT NULL,
            last_update TIMESTAMP NOT NULL,
            interval_start DATE NOT NULL,
            interval_end DATE NOT NULL,
            amount BIGINT NOT NULL,
            UNIQUE(spend_plan_id, batch_id, interval_start, interval_end)
          )
        """,
      sql"""
          CREATE UNIQUE INDEX IF NOT EXISTS SPEND_ALLOCATIONS_UNIQUE
          ON SPEND_ALLOCATIONS(
            spend_plan_id, batch_id, interval_start, interval_end
          )
        """
    ).traverse(_.update.run).map(_ => ())
}
