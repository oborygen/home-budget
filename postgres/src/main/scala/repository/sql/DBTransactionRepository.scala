package repository.sql

import java.util.UUID

import domain.ProcessedTransaction
import doobie._
import doobie.implicits._
import repository.TransactionRepository

//TODO: move to a module
class DBTransactionRepository extends TransactionRepository[ConnectionIO] {

  override def exists(id: String): ConnectionIO[Boolean] =
    sql"""
       select count(*)
       from PROCESSED_TRANSACTIONS
       where id = $id
    """
      .query[Int]
      .unique
      .map(_ > 0)

  override def isAllocated(id: String): doobie.ConnectionIO[Boolean] =
    sql"""
       select count(*)
       from PROCESSED_TRANSACTIONS
       where id = $id and allocated = TRUE
    """
      .query[Int]
      .unique
      .map(_ > 0)


  override def load(id: String): doobie.ConnectionIO[Option[ProcessedTransaction]] =
    sql"""
          select * from PROCESSED_TRANSACTIONS where id = $id
        """.query[ProcessedTransaction].option

  override def saveOrUpdate(transaction: ProcessedTransaction): ConnectionIO[Unit] = {
    val insert = sql"""
       insert into PROCESSED_TRANSACTIONS(id, transaction_date, booking_date, title, counterparty, amount, category_id, batch_id, allocated)
       values(${transaction.transaction.id},
              ${transaction.transaction.transactionDate},
              ${transaction.transaction.bookingDate},
              ${transaction.transaction.title},
              ${transaction.transaction.counterparty},
              ${transaction.transaction.value},
              ${transaction.categoryId},
              ${transaction.batchId},
              ${transaction.allocated}
              )
    """.update.run.map(_ => ())
    val update = sql"""
       UPDATE PROCESSED_TRANSACTIONS SET
         transaction_date = ${transaction.transaction.transactionDate},
         booking_date = ${transaction.transaction.bookingDate},
         title = ${transaction.transaction.title},
         counterparty = ${transaction.transaction.counterparty},
         amount = ${transaction.transaction.value},
         category_id = ${transaction.categoryId},
         batch_id = ${transaction.batchId},
         allocated = ${transaction.allocated}
       WHERE id = ${transaction.transaction.id}
    """.update.run.map(_ => ())
    for {
      shouldUpdate <- exists(transaction.transaction.id)
      _ <- if(shouldUpdate) {
        update
      } else {
        insert
      }
    } yield {
      ()
    }

  }

  override def deleteBatch(batchId: UUID): doobie.ConnectionIO[Int] =
    sql"""delete from PROCESSED_TRANSACTIONS where batch_id = $batchId""".update.run

  def initialize: ConnectionIO[Unit] =
    sql"""
          CREATE TABLE IF NOT EXISTS PROCESSED_TRANSACTIONS(
            id VARCHAR PRIMARY KEY,
            transaction_date DATE NOT NULL,
            booking_date DATE NOT NULL,
            title VARCHAR NOT NULL,
            counterparty VARCHAR NOT NULL,
            amount DECIMAL NOT NULL,
            category_id VARCHAR REFERENCES CATEGORIES ON DELETE RESTRICT ,
            batch_id VARCHAR NOT NULL REFERENCES BATCHES ON DELETE CASCADE,
            allocated BOOLEAN NOT NULL
          )
      """.update.run.map(_ => ())
}
