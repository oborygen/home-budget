package repository.sql

import java.util.UUID

import domain.CategorizationRule
import doobie._
import doobie.implicits._
import repository.RuleRepository

class DBRuleRepository extends RuleRepository[ConnectionIO] {

  private implicit val readRule: Read[CategorizationRule] = Read[(UUID, String, UUID)].map {
    case (id, expression, categoryId) => CategorizationRule(id, expression, categoryId)
  }

  override def loadRules(): ConnectionIO[List[CategorizationRule]] =
    sql"""
          select * from CATEGORIZATION_RULES
      """.query[CategorizationRule].to[List]

  override def addRule(rule: CategorizationRule): ConnectionIO[Unit] =
    sql"""
         insert into CATEGORIZATION_RULES(id, expression, category_id)
         values(${rule.id}, ${rule.expression}, ${rule.categoryId})
     """.update.run.map(_ => ())

  def initialize: ConnectionIO[Unit] =
    sql"""
          CREATE TABLE IF NOT EXISTS CATEGORIZATION_RULES(
            id VARCHAR PRIMARY KEY,
            expression VARCHAR,
            category_id VARCHAR REFERENCES CATEGORIES ON DELETE RESTRICT
          )
      """.update.run.map(_ => ())
}
