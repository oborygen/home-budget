package repository

import java.sql.Timestamp
import java.time.{LocalDate, LocalDateTime, ZoneId}
import java.util.UUID

import doobie._

package object sql {
  implicit val localDateGet: Get[LocalDate] =
    Get[Timestamp].map(_.toLocalDateTime.toLocalDate)
  implicit val localDatePut: Put[LocalDate] =
    Put[Timestamp].contramap(time => Timestamp.from(time.atStartOfDay(ZoneId.systemDefault()).toInstant))
  implicit val localDateTimeGet: Get[LocalDateTime] =
    Get[Timestamp].map(ts => LocalDateTime.ofInstant(ts.toInstant, ZoneId.systemDefault()))
  implicit val localDateTimePut: Put[LocalDateTime] =
    Put[Timestamp].contramap(time => Timestamp.from(time.atZone(ZoneId.systemDefault()).toInstant))
  implicit val uuidGet: Get[UUID] = Get[String].map(UUID.fromString)
  implicit val uuidPut: Put[UUID] = Put[String].contramap(_.toString)
}
