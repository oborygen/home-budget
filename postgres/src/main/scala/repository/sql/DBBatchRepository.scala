package repository.sql

import java.time.LocalDateTime
import java.util.UUID

import domain.Batch
import doobie.free.connection.ConnectionIO
import doobie.implicits._
import doobie.util.Read
import repository.BatchRepository

class DBBatchRepository extends BatchRepository[ConnectionIO] {

  implicit private val readRule: Read[Batch] = Read[(UUID, LocalDateTime, Int, Int, Int, Int, Int)].map {
    case (id, saveTime, transactionCount, existingCount, categorizedCount, unallocatedCount, allocatedCount) =>
      Batch(id, saveTime, transactionCount, existingCount, categorizedCount, unallocatedCount, allocatedCount)
  }

  override def list: ConnectionIO[List[Batch]] =
    sql"""select id, created, transaction_count, existing_count, categorized_count, unallocated_count, allocated_count
          from BATCHES""".query[Batch].to[List]

  override def save(batch: Batch): ConnectionIO[Unit] =
    sql"""insert into BATCHES(id, created, transaction_count, existing_count, categorized_count, unallocated_count, allocated_count)
          values(${batch.id}, ${batch.processingTime}, ${batch.transactionsCount}, ${batch.existingCount},
          ${batch.categorizedCount}, ${batch.unallocatedCount}, ${batch.allocatedCount})""".update.run.map(_ => ())

  override def delete(batchId: UUID): ConnectionIO[Unit] =
    sql"""delete from BATCHES where id = $batchId""".update.run.map(_ => ())

  def initialize: ConnectionIO[Unit] =
    sql"""
          CREATE TABLE IF NOT EXISTS BATCHES(
            id VARCHAR PRIMARY KEY,
            created TIMESTAMP,
            transaction_count INT,
            existing_count INT,
            categorized_count INT,
            unallocated_count INT,
            allocated_count INT
          )
      """.update.run.map(_ => ())
}
