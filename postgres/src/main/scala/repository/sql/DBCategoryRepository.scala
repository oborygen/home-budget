package repository.sql

import java.util.UUID

import cats.implicits._
import domain.Category
import doobie._
import doobie.implicits._
import repository.CategoryRepository

class DBCategoryRepository extends CategoryRepository[ConnectionIO] {

  implicit private val readCategory: Read[Category] =
    Read[(UUID, String, Option[UUID])].map {
      case (id, name, parent) => Category(id, name, parent)
    }


  override def load(id: UUID): doobie.ConnectionIO[Option[Category]] =
    sql"""
          select id, name, parent_id from CATEGORIES where id = $id
    """.query[Category].option

  override def allCategories: doobie.ConnectionIO[List[Category]] =
    sql"""
          select id, name, parent_id from CATEGORIES
    """.query[Category].to[List]

  override def save(category: Category): doobie.ConnectionIO[Unit] =
    for {
      _ <- sql"""
                  insert into CATEGORIES(id, name, parent_id)
                  values(${category.id}, ${category.name}, ${category.group})
                  on conflict(id) do update set name = ${category.name}
              """.update.run
    } yield {
      ()
    }


  def initialize: ConnectionIO[Unit] =
    List(
      sql"""
          CREATE TABLE IF NOT EXISTS CATEGORIES(
            id VARCHAR PRIMARY KEY,
            name VARCHAR NOT NULL,
            parent_id VARCHAR,
            UNIQUE(name, parent_id)
          )
        """
    ).traverse(_.update.run).map(_ => ())

}
