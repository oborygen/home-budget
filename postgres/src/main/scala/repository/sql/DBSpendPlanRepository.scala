package repository.sql

import java.time.LocalDate
import java.util.UUID

import cats.data.OptionT
import cats._
import cats.implicits._
import cats.data._
import domain.{Weekly, Yearly, _}
import cats.implicits._
import doobie._
import doobie.implicits._
import doobie.postgres._
import doobie.postgres.implicits._
import repository.SpendPlanRepository

class DBSpendPlanRepository(implicit H: ThrowableMonadError[ConnectionIO]) extends SpendPlanRepository[ConnectionIO] {

  private implicit val putInterval: Put[PlanningInterval] = Put[String].contramap {
    case Weekly => "Weekly"
    case Monthly => "Monthly"
    case Quarterly => "Quarterly"
    case Yearly => "Yearly"
  }
  private implicit val getInterval: Get[PlanningInterval] = Get[String].map {
    case "Weekly" => Weekly
    case "Monthly" => Monthly
    case "Quarterly" => Quarterly
    case "Yearly" => Yearly
  }

  override def load(id: UUID): ConnectionIO[Option[SpendPlan]] =
    (for {
      (start, interval, categories, amount) <- OptionT(
        sql"select start, interval, categories, amount from SPEND_PLANS where id = $id"
          .query[(LocalDate, PlanningInterval, List[UUID], BigDecimal)].option)
    } yield {
      SpendPlan(id, categories.toSet, start, interval, amount)
    }).value

  override def allPlans: doobie.ConnectionIO[Set[SpendPlan]] =
    sql"""
          select id, start, interval, amount, categories from SPEND_PLANS
      """
      .query[(UUID, LocalDate, PlanningInterval, BigDecimal, List[UUID])]
      .map { case (id, date, interval, amount, categories) =>
        SpendPlan(id, categories.toSet, date, interval, amount)
      }
      .to[Set]

  override def save(spendPlan: SpendPlan): ConnectionIO[Unit] =
    spendPlan.categories.toList match {
      case head :: tail =>
        save(spendPlan, NonEmptyList(head, tail))
      case _ =>
        H.raiseError(new RuntimeException("List of categories cannot be empty"))
    }

  private def save(spendPlan: SpendPlan, categories: NonEmptyList[UUID]) =
    for {
      categories <- (fr"""select id from CATEGORIES where """ ++ Fragments.in(fr"id", categories)).query[UUID].to[List]
      _ <- if(spendPlan.categories.forall(categories.contains)) {
        sql"""
                  insert into SPEND_PLANS(id, start, interval, amount, categories)
                  values(${spendPlan.id}, ${spendPlan.start}, ${spendPlan.interval}, ${spendPlan.amount}, ${spendPlan.categories.toList})
              """.update.run
      } else {
        H.raiseError(new RuntimeException("Undefined categories"))
      }
    } yield {
      ()
    }


  def initialize: doobie.ConnectionIO[Unit] =
    List(sql"""
              CREATE TABLE IF NOT EXISTS SPEND_PLANS(
                id VARCHAR primary key,
                start DATE,
                interval VARCHAR NOT NULL,
                amount DECIMAL NOT NULL,
                categories VARCHAR[] NOT NULL
              )
            """).traverse(_.update.run).map(_ => ())
}
