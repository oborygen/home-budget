# home-budget

Home budget planning. Parses banks extracts and allocates spends to categories, reports summaries, tracks budget levels.

## Transaction allocation processing:

1. Generate next batch ID - UUID or sequence.
1. Read next set of transactions.
1. Read and initialize categorization rules.
1. For each transaction:
   - verify if is not duplicated in the index (use transaction ID or a synthetic ID based on fingerprint) - drop if duplicated 
   - categorize
   - find dedicated `SpendPlan` based on category
   - load `SpendAllocation` for given `SpendPlan` and transaction date.
   - if `SpendAllocation` doesn't exist - create one
   - update allocation
   - store transaction associated with category and batch ID - updates transactions index
1. Store updated allocations associated with batch ID.

## Spend allocation indexing

 Each allocation is indexed by 3 values:
     - `SpendPlan` identifier
     - `startDate` must be <= `Transaction.transactionDate` 
     - `endDate` must be >= `Transaction.transactionDate`
     - batch ID - there can be multiple batches modifying given allocation.

When loading allocation during transactions processing the system needs to identify the latest allocation for given triplet.

In case there are multiple allocations this means they were updated during different batches. In such case the system should get the one with the latest `batch.startDate`.

# Starting database in Docker

```bash
docker run --name budget-postgres -e POSTGRES_PASSWORD=test -v /Users/b.jankiewicz/Devel/analiza-wydatkow/data:/var/lib/postgresql/data -p 5432:5432 postgres
```