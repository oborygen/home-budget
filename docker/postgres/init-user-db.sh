#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER home_budget;
    CREATE DATABASE home_budget;
    GRANT ALL PRIVILEGES ON DATABASE home_budget TO home_budget;
EOSQL