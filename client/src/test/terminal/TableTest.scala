package terminal

import java.time.LocalDate
import java.util.UUID

import domain._
import terminal.Formatter.LEFT
import utest._

object TableTest extends TestSuite {
  override def tests: Tests = Tests {
    val plans = List(
      SpendPlan(UUID.randomUUID, Set(UUID.randomUUID), LocalDate.now, Monthly, 123.00),
      SpendPlan(UUID.randomUUID, Set(UUID.randomUUID), LocalDate.now, Yearly, 123.00),
      SpendPlan(UUID.randomUUID, Set(UUID.randomUUID), LocalDate.now, Monthly, 1000.00),
      SpendPlan(UUID.randomUUID, Set(UUID.randomUUID), LocalDate.now, Monthly, 123.00),
      SpendPlan(UUID.randomUUID, Set(UUID.randomUUID), LocalDate.now, Yearly, 12123.00)
    )

    implicit val intervalFormatter = new ValueFormatter[PlanningInterval] {
      override def format(t: PlanningInterval): String = t match {
        case Weekly => "Weekly"
        case Monthly => "Monthly"
        case Quarterly => "Quarterly"
        case Yearly => "Yearly"
      }
      override def alignment: Formatter.Alignment = LEFT
    }

    val table = Table.render[SpendPlan](List("id", "interval", "amount", "start"), plans)(
      CellRenderer[SpendPlan, UUID](_.id),
      CellRenderer[SpendPlan, PlanningInterval](_.interval),
      CellRenderer[SpendPlan, BigDecimal](_.amount),
      CellRenderer[SpendPlan, LocalDate](_.start)
    )
    println(table)
  }
}
