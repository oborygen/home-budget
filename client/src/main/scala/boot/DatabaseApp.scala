package boot

import cats.implicits._
import cats.effect._
import doobie._
import doobie.implicits._
import doobie.hikari.HikariTransactor
import repository.sql.{DBAllocationRepository, DBBatchRepository, DBCategoryRepository, DBRuleRepository, DBSpendPlanRepository, DBTransactionRepository}
import services.{BatchService, CategorizationRuleService}

trait DatabaseApp { self: IOApp with Config =>

  val transactor: Resource[IO, HikariTransactor[IO]] =
    for {
      ce <- ExecutionContexts.fixedThreadPool[IO](4)
      te <- ExecutionContexts.cachedThreadPool[IO]
      xa <- HikariTransactor.newHikariTransactor[IO](
        settings.database.driver,
        settings.database.url,
        settings.database.user,
        settings.database.password.value,
        ce, // await connection here
        te // execute JDBC operations here
      )
    } yield xa

  implicit val allocationRepository = new DBAllocationRepository
  implicit val transactionRepository = new DBTransactionRepository
  implicit val ruleRepository = new DBRuleRepository
  implicit val spendPlanRepository = new DBSpendPlanRepository
  implicit val categoryRepository = new DBCategoryRepository
  implicit val batchRepository = new DBBatchRepository

  def initialize: ConnectionIO[Unit] =
    List(
      categoryRepository.initialize,
      spendPlanRepository.initialize,
      allocationRepository.initialize,
      batchRepository.initialize,
      transactionRepository.initialize,
      ruleRepository.initialize
    ).sequence.map(_ => println("Tables created")).handleError {
      t: Throwable =>
        println(t.getMessage)
        t.printStackTrace()
    }

}
