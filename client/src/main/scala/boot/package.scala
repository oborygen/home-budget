import java.time.LocalDate
import java.util.UUID

import domain._
import scopt.Read
import scopt.Read._

package object boot {

  implicit val uuidRead: Read[UUID] = reads { str =>
    UUID.fromString(str)
  }
  implicit val localDateRead: Read[LocalDate] = reads { str =>
    LocalDate.parse(str)
  }
  implicit val planningIntervalRead: Read[PlanningInterval] = reads {
    case "Weekly" => Weekly
    case "Monthly" => Monthly
    case "Quarterly" => Quarterly
    case "Yearly" => Yearly
  }

}
