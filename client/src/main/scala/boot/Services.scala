package boot

import domain.Transaction
import doobie._
import doobie.implicits._
import rules.{CategoryId, Rule}
import services.{BatchService, CategorizationRuleService}

trait Services { self: DatabaseApp =>

  private def categorization: ConnectionIO[Transaction => Option[CategoryId]] = {
    for {
      rules <- CategorizationRuleService.loadRules()
    } yield {
      rules match {
        case Nil => _ => None
        case someRules => someRules.reduce(_ orElse _).lift
      }
    }
  }

  implicit lazy val batchService: doobie.ConnectionIO[BatchService[doobie.ConnectionIO]] =
    for {
      categorize <- categorization
    } yield {
      new BatchService[ConnectionIO](categorize)
    }

}
