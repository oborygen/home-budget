package boot


import cats.effect._
import cats.implicits._
import doobie.implicits._

object Start extends IOApp with Config with DatabaseApp with Services with Actions {
  override def run(args: List[String]): IO[ExitCode] = {
    transactor.use { implicit xa =>
      program.run(args).transact(xa)
    }.map(_ => ExitCode.Success).handleError {
      err: Throwable => {
        print(err.getMessage)
        err.printStackTrace()
        ExitCode.Error
      }
    }
  }
}
