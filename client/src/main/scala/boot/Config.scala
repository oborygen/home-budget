package boot

import boot.config.AppConfig
import com.typesafe.scalalogging.LazyLogging
import pureconfig.error.ConfigReaderFailures
import pureconfig.generic.auto._

trait Config extends LazyLogging {

  val settings: AppConfig = {
    pureconfig.loadConfig[AppConfig] match {
      case Left(failures) =>
        logFailures(failures)
        throw new IllegalStateException("Config format error: " + failures)
      case Right(value) =>  {
        logger.info(value.toString)
        value
      }
    }
  }

  private def logFailures(failures: ConfigReaderFailures): Unit = {
    failures.toList.foreach(failure => logger.error(failure.description))
  }

}
