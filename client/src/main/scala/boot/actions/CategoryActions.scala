package boot.actions

import java.util.UUID

import boot.actions.Action._
import cats.implicits._
import domain._
import repository.CategoryRepository
import terminal.{CellRenderer, Table}

class CategoryActions[M[_]](implicit repo: CategoryRepository[M], H: ThrowableMonadError[M]) {

  import CategoryActions._

  val add: ActionRoutes[M] = Action[M, Category] {
    case "category" / "add" => category =>
      for {
        _ <- repo.save(category)
      } yield {
        println(s"Category saved with ID ${category.id}")
      }
  }

  val update: ActionRoutes[M] = Action[M, Category] {
    case "category" / "update" => category =>
      for {
        _ <- repo.save(category)
      } yield {
        println(s"Category ${category.id} updated")
      }
  }

  val list: ActionRoutes[M] = Action[M, Unit] {
    case "category" / "list" => _ =>
      for {
        categories <- repo.allCategories
      } yield {
        val catmap = categories.map(c => c.id -> c).toMap
        val categoriesWithGroups = categories.map { category =>
          category -> category.group.map(catmap).map(_.name)
        }
        printCategories(categoriesWithGroups)
      }
  }

  val actions = add <+> list <+> update

}

object CategoryActions {
  implicit val categoryParser: InputParser[Category] = input => {
    input.categoryName.map { name =>
      Category(input.categoryId.getOrElse(UUID.randomUUID), name, input.categoryGroup)
    }
  }

  def printCategories(categories: List[(Category, Option[String])]): Unit = println(
    Table.render[(Category, Option[String])](List("Id", "Name", "Parent ID", "Parent"), categories)(
      CellRenderer[(Category, Option[String]), UUID](_._1.id),
      CellRenderer[(Category, Option[String]), String](_._1.name),
      CellRenderer[(Category, Option[String]), Option[UUID]](_._1.group),
      CellRenderer[(Category, Option[String]), String](_._2.getOrElse("-"))
    )
  )

}