package boot.actions

import boot.actions.Action._
import domain.ThrowableMonadError

class AdminActions[M[_]](init: => M[Unit])(implicit H: ThrowableMonadError[M]) {

  val initialize = Action[M, Unit] {
    case "admin" / "init" => _ => {
      init
    }
  }

  val actions = initialize


}
