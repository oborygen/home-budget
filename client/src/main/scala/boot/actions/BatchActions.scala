package boot.actions

import java.io.File

import boot.actions.Action._
import cats.data.Validated
import cats.implicits._
import domain._
import parsers.IngTransactionsParser
import repository.BatchRepository
import services.BatchService
import terminal.{CellRenderer, Table}

import scala.io.Source

class BatchActions[M[_]: BatchRepository](implicit serviceM: M[BatchService[M]], H: ThrowableMonadError[M]) {

  import BatchActions._

  val process: ActionRoutes[M] = Action[M, File] {
    case "batch" / "process" =>
      path =>
        for {
          service <- serviceM
          lines = Source.fromFile(path)(IngTransactionsParser.defaultEncoding).getLines()
          transactions = IngTransactionsParser.parse(lines.toStream)
          result <- service.processBatch(transactions.toStream)
          _ <- service.saveBatch(result)
        } yield {
          println("Allocated plans count: " + result.allocations.size)
          println("Allocated plans: " + result.allocations.map(_._1.spendPlanId).mkString(", "))
          println("Unallocated transactions count: " + result.unallocated.size)
          println("Uncategorized transactions count: " + result.uncategorized.size)
          println("Categorized transactions count: " + result.allocations.flatMap(_._2).size)
          println(
            "Categorized transactions value: " + result.allocations
              .flatMap(_._2)
              .map(_.transaction.value)
              .sum)
          println("Uncategorized transactions IDs: " + result.uncategorized.map(_.transaction.id).mkString(", "))
        }
  }

  val list: ActionRoutes[M] = Action[M, Unit] {
    case "batch" / "list" => _ =>
      for {
        batches <- BatchRepository.syntax.list
      } yield {
        println(Table.render[Batch]("id", "Date", "Transactions", "Existing", "Categorized", "Allocated")(batches)(
          CellRenderer(b => b.id),
          CellRenderer(b => b.processingTime),
          CellRenderer(b => b.transactionsCount),
          CellRenderer(b => b.existingCount),
          CellRenderer(b => b.categorizedCount),
          CellRenderer(b => b.allocatedCount)
        ))
      }
  }

  val actions = process <+> list
}

object BatchActions {
  implicit val fileInput: InputParser[File] = i =>
    Validated.fromOption(i.file, "Transaction file path option is missing")
}
