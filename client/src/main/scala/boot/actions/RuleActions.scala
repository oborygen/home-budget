package boot.actions

import java.util.UUID

import Action._
import cats.data._
import cats.implicits._
import domain.{CategorizationRule, Category, ThrowableMonadError}
import repository.{CategoryRepository, RuleRepository}
import services.CategorizationRuleService
import terminal.{CellRenderer, Table}

class RuleActions[M[_] : RuleRepository : CategoryRepository](implicit H: ThrowableMonadError[M]) {

  import RuleActions._

  val add: ActionRoutes[M] = Action[M, CategorizationRule] {
    case "rule" / "add" => rule =>
      for {
        _ <- CategorizationRuleService.addRule(rule)
      } yield {
        println(s"Rule has been added with id = ${rule.id}")
      }
  }

  val list: ActionRoutes[M] = Action[M, Unit] {
    case "rule" / "list" => _ =>
      import repository.RuleRepository.syntax._
      import repository.CategoryRepository.syntax._
      for {
        rules <- loadRules
        categoriesExpanded <- rules.map(rule => loadCategory(rule.categoryId).map(rule -> _)).sequence
      } yield {
        print(Table.render[(CategorizationRule, Option[Category])]("Id", "Expression", "Category ID", "Category")(categoriesExpanded)(
          CellRenderer[(CategorizationRule, Option[Category]), UUID](_._1.id),
          CellRenderer[(CategorizationRule, Option[Category]), String](_._1.expression),
          CellRenderer[(CategorizationRule, Option[Category]), UUID](_._1.categoryId),
          CellRenderer[(CategorizationRule, Option[Category]), String](_._2.map(_.name).getOrElse("Unknown"))
        ))
      }
  }

  val actions: ActionRoutes[M] = add <+> list

}

object RuleActions {

  implicit val inputParser: InputParser[CategorizationRule] = input => {
    (
      Validated.fromOption(input.ruleCategory, "Missing category ID option"),
      Validated.fromOption(input.ruleExpression, "Missing expression option")
    ).mapN((categoryId, expression) => CategorizationRule(UUID.randomUUID(), expression, categoryId))

  }

}
