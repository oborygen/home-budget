package boot.actions

import boot.InputArguments
import cats.data.Validated.{Invalid, Valid}
import cats.data.{Kleisli, OptionT, Validated}
import cats.implicits._
import domain.ThrowableMonadError

object Action {

  type InputParser[I] = InputArguments => Validated[String, I]

  // For ? see https://github.com/non/kind-projector
  type ActionRoutes[M[_]] = Kleisli[OptionT[M, ?], InputArguments, Unit]

  implicit val unitInputParser: InputParser[Unit] = _ => ().valid

  final case class /(action: String, subAction: String)

  def apply[M[_], I](pf: PartialFunction[/, I => M[Unit]])(
      implicit inputParser: InputParser[I],
      H: ThrowableMonadError[M]): ActionRoutes[M] =
    Kleisli { i =>
      OptionT((for {
        action <- i.action
        subAction <- i.subAction
      } yield {
        val path = /(action, subAction)
        if (pf.isDefinedAt(path)) {
          Some(inputParser(i) match {
            case Valid(data) => pf(path)(data)
            case Invalid(e) => H.raiseError[Unit](new RuntimeException(s"Invalid input for action $path : $e"))
          })
        } else {
          None
        }
      }).flatten.sequence)
    }

}
