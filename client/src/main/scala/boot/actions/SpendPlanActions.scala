package boot.actions

import java.time.LocalDate
import java.util.UUID

import boot.actions.Action._
import cats.implicits._
import domain._
import repository.SpendPlanRepository
import terminal.Formatter.LEFT
import terminal.{CellRenderer, Formatter, Table, ValueFormatter}

class SpendPlanActions[M[_]](implicit spr: SpendPlanRepository[M], H: ThrowableMonadError[M]) {

  import SpendPlanActions._

  val savePlan: ActionRoutes[M] = Action[M, SpendPlan] {
    case "spend-plan" / "add" =>
      plan =>
        for {
          _ <- SpendPlanRepository.syntax.save(plan)
        } yield {
          println(s"Spend plan saved with ID ${plan.id}")
        }
  }

  val listPlans: ActionRoutes[M] = Action[M, Unit] {
    case "spend-plan" / "list" =>
      _ =>
        for {
          plans <- SpendPlanRepository.syntax.allPlans
        } yield {
          printPlans(plans)
        }
  }

  val actions: ActionRoutes[M] = savePlan <+> listPlans

}

object SpendPlanActions {

  implicit val inputParser: InputParser[SpendPlan] = input =>
    (input.categories, input.start, input.interval, input.amount).mapN { (categories, start, interval, amount) =>
      SpendPlan(UUID.randomUUID, categories, start, interval, amount)
  }

  implicit val intervalFormatter: ValueFormatter[PlanningInterval] = new ValueFormatter[PlanningInterval] {
    override def format(t: PlanningInterval): String = t match {
      case Weekly    => "Weekly"
      case Monthly   => "Monthly"
      case Quarterly => "Quarterly"
      case Yearly    => "Yearly"
    }
    override def alignment: Formatter.Alignment = LEFT
  }

  def printPlans(plans: Set[SpendPlan]) = println(
    Table.render[SpendPlan](List("Id", "Interval", "Categories", "Amount", "Start"), plans.toList)(
      CellRenderer[SpendPlan, UUID](_.id),
      CellRenderer[SpendPlan, PlanningInterval](_.interval),
      CellRenderer[SpendPlan, String](_.categories.mkString(", ")),
      CellRenderer[SpendPlan, BigDecimal](_.amount),
      CellRenderer[SpendPlan, LocalDate](_.start)
    )
  )

}
