package boot

import java.io.File
import java.time.LocalDate
import java.util.UUID

import boot.actions.Action.ActionRoutes
import buildinfo.BuildInfo
import cats.data.{OptionT, Validated}
import cats.implicits._
import domain._
import scopt.OParser

case class InputArguments(
    action: Option[String] = None,
    subAction: Option[String] = None,
    categories: Validated[String, Set[UUID]] = "Missing categories value".invalid,
    start: Validated[String, LocalDate] = "Missing start value".invalid,
    interval: Validated[String, PlanningInterval] = "Missing interval value".invalid,
    amount: Validated[String, BigDecimal] = "Missing amount value".invalid,
    categoryId: Option[UUID] = None,
    categoryName: Validated[String, String] = "Missing category name".invalid,
    categoryGroup: Option[UUID] = None,
    ruleExpression: Option[String] = None,
    ruleCategory: Option[UUID] = None,
    file: Option[File] = None
)

class Program[M[_]](actions: ActionRoutes[M])(implicit H: ThrowableMonadError[M]) {

  val parser: OParser[Unit, InputArguments] = {
    val builder = OParser.builder[InputArguments]
    import builder._
    OParser.sequence(
      programName("Home budget"),
      version(BuildInfo.version).hidden(),
      head("Home Budget", BuildInfo.version),
      cmd("spend-plan")
        .text("Spend plan actions")
        .action((_, c) => c.copy(action = "spend-plan".some))
        .children(
          cmd("add")
            .text("Add spend plan")
            .action((_, c) => c.copy(subAction = "add".some))
            .children(
              opt[Seq[UUID]]("categories").abbr("cids").action((v, c) => c.copy(categories = v.toSet.valid)),
              opt[LocalDate]("start").abbr("s").action((v, c) => c.copy(start = v.valid)),
              opt[PlanningInterval]("interval").abbr("i").action((v, c) => c.copy(interval = v.valid)),
              opt[BigDecimal]("amount").abbr("a").action((v, c) => c.copy(amount = v.valid))
            ),
          cmd("list")
            .text("List spend plans")
            .action((_, c) => c.copy(subAction = "list".some))
        ),
      cmd("category")
        .text("Categories actions")
        .action((_, c) => c.copy(action = "category".some))
        .children(
          cmd("add")
            .text("Add category")
            .action((_, c) => c.copy(subAction = "add".some))
            .children(
              opt[UUID]("parent").abbr("p").optional().action((v, c) => c.copy(categoryGroup = v.some)),
              opt[String]("name").abbr("n").action((v, c) => c.copy(categoryName = v.valid))
            ),
          cmd("update")
            .text("Update category")
            .action((_, c) => c.copy(subAction = "update".some))
            .children(
              opt[UUID]("id").abbr("id").optional().action((v, c) => c.copy(categoryId = v.some)),
              opt[UUID]("parent").abbr("p").optional().action((v, c) => c.copy(categoryGroup = v.some)),
              opt[String]("name").abbr("n").action((v, c) => c.copy(categoryName = v.valid))
            ),
          cmd("list")
            .text("List categories")
            .action((_, c) => c.copy(subAction = "list".some))
        ),
      cmd("rule")
        .text("Rule actions")
        .action((_, c) => c.copy(action = "rule".some))
        .children(
          cmd("add")
            .text("Add categorization rule")
            .action((_, c) => c.copy(subAction = "add".some))
            .children(
              opt[String]("expression").abbr("exp").action((v, c) => c.copy(ruleExpression = v.some)),
              opt[UUID]("categoryId").abbr("cid").action((v, c) => c.copy(ruleCategory = v.some))
            ),
          cmd("list")
            .text("List rules")
            .action((_, c) => c.copy(subAction = "list".some))
        ),
      cmd("batch")
        .text("Transaction batch processing - import and previous imports history reports")
        .action((_, c) => c.copy(action = "batch".some))
        .children(
          cmd("process")
            .text("Process new bank extract")
            .action((_, c) => c.copy(subAction = "process".some))
            .children(
              opt[File]("file").abbr("f").action((v, c) => c.copy(file = v.some))
            ),
          cmd("list")
            .text("List previous imports")
            .action((_, c) => c.copy(subAction = "list".some))
        ),
      cmd("admin")
        .text("Administrative actions")
        .action((_, c) => c.copy(action = "admin".some))
        .children(
          cmd("init")
            .text("Initialize database")
            .action((_, c) => c.copy(subAction = "init".some))
        )
    )
  }

  def run(args: Seq[String]): M[Unit] =
    OParser.parse(parser, args, InputArguments()) match {
      case None =>
        H.pure(OParser.usage(parser))
      case Some(input) =>
        actions(input).getOrElse(
          OptionT.pure[M](
            H.pure(println("Unimplemented action"))
          ))
      case _ =>
        H.pure(println(OParser.usage(parser)))
    }

}
