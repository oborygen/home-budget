package boot

import boot.actions.Action.ActionRoutes
import cats.implicits._
import doobie.implicits._
import boot.actions._
import doobie.free.connection.ConnectionIO

trait Actions { self: DatabaseApp with Services =>

  private val actions: ActionRoutes[ConnectionIO] = {
    List(
      new SpendPlanActions[ConnectionIO].actions,
      new CategoryActions[ConnectionIO].actions,
      new BatchActions[ConnectionIO].actions,
      new RuleActions[ConnectionIO].actions,
      new AdminActions[ConnectionIO](initialize).actions
    ).reduce(_ <+> _)
  }

  val program = new Program[ConnectionIO](actions)
}
