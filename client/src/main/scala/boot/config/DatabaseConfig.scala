package boot.config

case class DatabaseConfig(
    driver: String,
    url: String,
    user: String,
    password: Sensitive
)
