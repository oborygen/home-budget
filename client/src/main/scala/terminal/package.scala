import java.time.format.DateTimeFormatter
import java.time.{LocalDate, LocalDateTime}
import java.util.UUID

import terminal.Formatter._

package object terminal {

  def rightPadded(value: String, space: Int): String = value + " " * (space - value.length)
  def leftPadded(value: String, space: Int): String = " " * (space - value.length) + value
  def centered(value: String, space: Int): String = {
    val front: Int = (space - value.length) / 2
    val back: Int = space - value.length - front
    " " * front + value + " " * back
  }

  implicit val stringFormatter: ValueFormatter[String] = new ValueFormatter[String] {
    override def format(t: String): String = t
    override def alignment: Formatter.Alignment = LEFT
  }

  implicit val uuidFormatter: ValueFormatter[UUID] = new ValueFormatter[UUID] {
    override def format(t: UUID): String = t.toString
    override def alignment: Formatter.Alignment = LEFT
  }

  implicit val intFormatter: ValueFormatter[Int] = new ValueFormatter[Int] {
    override def format(t: Int): String = t.toString
    override def alignment: Alignment = RIGHT
  }

  implicit val bigDecimalFormatter: ValueFormatter[BigDecimal] = new ValueFormatter[BigDecimal] {
    override def format(t: BigDecimal): String = t.toString
    override def alignment: Alignment = RIGHT
  }

  implicit val localDateFormatter: ValueFormatter[LocalDate] = new ValueFormatter[LocalDate] {
    private val format = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    override def format(t: LocalDate): String = format.format(t)
    override def alignment: Alignment = CENTER
  }

  implicit val localDateTimeFormatter: ValueFormatter[LocalDateTime] = new ValueFormatter[LocalDateTime] {
    private val format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
    override def format(t: LocalDateTime): String = format.format(t)
    override def alignment: Alignment = CENTER
  }

  implicit def optionFormatter[F](implicit vf: ValueFormatter[F]): ValueFormatter[Option[F]] = new ValueFormatter[Option[F]] {
    override def format(t: Option[F]): String = t.fold("-")(vf.format)
    override def alignment: Alignment = vf.alignment
  }

}
