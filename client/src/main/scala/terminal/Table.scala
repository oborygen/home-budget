package terminal

object Table {

  val vbe = "║" // vertical border external
  val vbi = "║" // vertical border internal
  val lb = "║" // left border
  val rb = "║" // right border
  val ib = "║" // internal vertical border
  val hbe = "═" // horizontal border external
  val hbi = "═" // horizontal border internal
  val con = "╬" // internal connector
  val tl = "╔" // top-left connector
  val tr = "╗" // top-right connector
  val bl = "╚" // bottom-left connector
  val br = "╝" // bottom-right connector
  val lcon = "╠" // left external connector
  val rcon = "╣" // left external connector
  val tcon = "╦" // top external connector
  val bcon = "╩" // bottom external connector

  def render[A](columnNames: String*)(data: List[A])(cellRenderers: CellRenderer[A, _]*): String = {
    render(columnNames.toList, data)(cellRenderers: _*)
  }

  def render[A](columnNames: List[String], data: List[A])(cellRenderers: CellRenderer[A, _]*): String = {
    val sizes: List[Int] = {
      val initial = columnNames.map(name => name.length + 2)

      data.foldLeft(initial) { (maxSizes, next) =>
        val sizes = cellRenderers.toList.map(_.length(next))
        val res = (maxSizes zip sizes).map { case (x, y) => x max y }
        res
      }
    }

    val tobBorder: String = tl + sizes.map(size => hbe * size).mkString(tcon) + tr
    val bottomBorder: String = bl + sizes.map(size => hbe * size).mkString(bcon) + br

    val insideHorizontalBorder: String = lcon + sizes.map(size => hbe * size).mkString(con) + rcon

    def print(): String =
      tobBorder + "\n" +
        printHeader + "\n" +
        insideHorizontalBorder + "\n" +
        printRows + "\n" +
        bottomBorder


    def printHeader: String =
      lb + columnNames.zip(sizes).map { case (value, size) => centered(value, size) }.mkString(ib) + rb

    def printRow(item: A): String = {
      val values = cellRenderers.zip(sizes).map { case (r, size) => centered(r.render(size - 2, item), size) }
      lb + values.mkString(ib) + rb
    }

    def printRows: String = {
      data match {
        case Nil => lb + centered("no data", sizes.sum + sizes.length - 1) + rb
        case _ => data.map(printRow).mkString("\n")
      }
    }

    print()
  }

  implicit def mapping2CellRenderer[A, F](f: A => F)(implicit vf: ValueFormatter[F]): CellRenderer[A, F] =
    new CellRenderer[A, F](f)

}

