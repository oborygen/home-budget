package terminal

import terminal.Formatter.Alignment

trait ValueFormatter[T] {

  def format(t: T): String

  def alignment: Alignment
}

object Formatter {
  trait Alignment
  case object LEFT extends Alignment
  case object RIGHT extends Alignment
  case object CENTER extends Alignment
}