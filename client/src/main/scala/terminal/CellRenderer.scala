package terminal

import terminal.Formatter._

class CellRenderer[T, F](f: T => F)(implicit formatter: ValueFormatter[F]) {

  private def align(space: Int, out: String): String = formatter.alignment match {
    case LEFT => rightPadded(out, space)
    case RIGHT => leftPadded(out, space)
    case CENTER => centered(out, space)
  }

  def render(space: Int, data: T): String = align(space, formatter.format(f(data)))

  def length(data: T): Int = formatter.format(f(data)).length + 2

}

object CellRenderer {
  def apply[T, F](f: T => F)(implicit formatter: ValueFormatter[F]): CellRenderer[T, F] = new CellRenderer(f)
}