resolvers += Resolver.bintrayIvyRepo("rtimush", "sbt-plugin-snapshots")
resolvers += Resolver.sonatypeRepo("releases")

addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.9")

addSbtPlugin("com.github.gseitz" % "sbt-release" % "1.0.9")

addSbtPlugin("com.timushev.sbt" % "sbt-updates" % "0.3.4")

addSbtPlugin("ch.epfl.scala" % "sbt-scalafix" % "0.9.0")

addSbtPlugin("com.eed3si9n" % "sbt-buildinfo" % "0.7.0")

addCompilerPlugin("org.spire-math" %% "kind-projector" % "0.9.8")