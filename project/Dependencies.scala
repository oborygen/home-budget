import sbt._

object Dependencies {

  val circeVersion = "0.11.0"

  val doobieVersion = "0.6.0"

  val logback = "ch.qos.logback" % "logback-classic" % "1.2.3"
  val logging = "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2"
  val catsCore = "org.typelevel" %% "cats-core" % "1.5.0"
  val catsEffect = "org.typelevel" %% "cats-effect" % "1.1.0"
  val catsMtl = "org.typelevel" %% "cats-mtl-core" % "0.4.0"
  val parboiled = "org.parboiled" %% "parboiled" % "2.1.5"
  val circeCore = "io.circe" %% "circe-core" % circeVersion
  val circeGeneric = "io.circe" %% "circe-generic" % circeVersion
  val circeParser = "io.circe" %% "circe-parser" % circeVersion
  val doobieCore = "org.tpolecat" %% "doobie-core" % doobieVersion
  val doobieH2 = "org.tpolecat" %% "doobie-h2" % doobieVersion
  val doobieHikari = "org.tpolecat" %% "doobie-hikari" % doobieVersion
  val doobiePostgres = "org.tpolecat" %% "doobie-postgres" % doobieVersion
  val pureConfig = "com.github.pureconfig" %% "pureconfig" % "0.10.1"
  val scopt = "com.github.scopt" %% "scopt" % "4.0.0-RC2"

  val utest = "com.lihaoyi" %% "utest" % "0.6.6"
  val scalaCheck = "org.scalacheck" %% "scalacheck" % "1.14.0"
  val scalaCheckDateTime = "com.47deg" %% "scalacheck-toolbox-datetime" % "0.2.5"
  val doobieSpecs2 = "org.tpolecat" %% "doobie-specs2" % doobieVersion

  val testContainers = "org.testcontainers" % "postgresql" % "1.10.5"

  val commonDependencies = Seq(
     catsCore, catsEffect, catsMtl
  )

  val coreDependencies = Seq(
    logging, parboiled, circeCore, circeGeneric,
    circeParser, doobieCore, doobieH2, doobieHikari, doobiePostgres
  )

  val terminalDependencies = Seq(
    catsCore, catsEffect, pureConfig, scopt, logback, logging
  )

  val postgresDependencies = Seq(
    doobieCore, doobiePostgres
  )

  val testDependencies = Seq(
    utest, scalaCheck, scalaCheckDateTime, doobieSpecs2
  ).map(_ % "it,test")

  val itDependencies = Seq(
    testContainers
  ).map(_ % "it,test")

}
